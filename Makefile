include .env
export
migrate:
	docker-compose run app pipenv run python3 src/manage.py migrate $(if $m, api $m,)

makemigrations:
	pipenv run python3 src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	pipenv run python3 src/manage.py createsuperuser

collectstatic:
	docker-compose run app pipenv run python3 src/manage.py collectstatic --no-input

dev:
	pipenv run python3 src/manage.py runserver

command:
	pipenv run python3 src/manage.py ${c}

shell:
	pipenv run python3 src/manage.py shell

debug:
	pipenv run python3 src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	pipenv run isort .
	pipenv run flake8 --config setup.cfg
	pipenv run black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

up:
	docker-compose up -d

down:
	docker-compose down --remove-orphans

build:
	docker-compose build

docker_dev:
	docker-compose up -d app
	docker-compose run app pipenv run python3 src/manage.py runserver 0.0.0.0:8000

docker_command:
	docker-compose up -d mydb
	docker-compose run app pipenv run python3 src/manage.py ${command}

docker_createsuperuser:
	docker-compose run app pipenv run python3 src/manage.py createsuperuser

up_db:
	docker-compose up -d mydb

docker_makemigrations:
	docker-compose run app pipenv run python3 src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

docker_lint:
	docker-compose run app pipenv run isort --check --diff .
	docker-compose run app pipenv run flake8 --config setup.cfg
	docker-compose run app pipenv run black --check --config pyproject.toml .

test:
	docker-compose run app bash -c "cd src && pipenv run pytest"

start_bot:
	pipenv run python3 src/manage.py bot

migrate_bot:
	pipenv run python3 src/manage.py migrate