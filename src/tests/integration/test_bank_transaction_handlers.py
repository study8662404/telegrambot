import pytest

from app.internal.bank_transactions.presentation.messages import (
    BANK_TRANSACTION_HELP_MESSAGE,
    BANK_TRANSACTION_INCORRECT_SUM_MESSAGE,
    BANK_TRANSACTION_NOT_ELECTED_INCORRECT_MESSAGE,
    BANK_TRANSACTION_NOT_RECEIVER_ACCOUNT_MESSAGE,
    BANK_TRANSACTION_NOT_REGISTER_SENDER_MESSAGE,
    BANK_TRANSACTION_RECEIVER_INCORRECT_MESSAGE,
    BANK_TRANSACTION_SENDER_INCORRECT_MESSAGE,
    BANK_TRANSACTION_SEVERAL_RECEIVER_ACCOUNTS_MESSAGE,
    BANK_TRANSACTION_SEVERAL_SENDER_ACCOUNTS_MESSAGE,
    BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE,
    BANK_TRANSACTION_SUCCESS_MESSAGE,
    BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE,
    BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE,
    NO_BANK_ACCOUNT,
)
from tests.integration.conftest import set_args_to_mock, test_bank_transaction_handler


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_incorrect_length_of_args(update_mock, context_mock):
    args = ["arg1", "arg2", "arg3", "arg4"]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=BANK_TRANSACTION_HELP_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_empty_args(update_mock, context_mock):
    test_bank_transaction_handler.do_transaction(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=BANK_TRANSACTION_HELP_MESSAGE
    )


##############################################################################################
# Тесты для команды do_transaction с 2 аргументами
##############################################################################################


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_name_but_not_in_elected_list(
    update_mock_from_another_person, context_mock, test_another_bank_account
):
    test_amount = "100"
    args = [update_mock_from_another_person.effective_user.username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_mock_from_another_person, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_another_person.effective_chat.id, text=BANK_TRANSACTION_NOT_ELECTED_INCORRECT_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_name(
    update_with_elected_mock, context_mock, test_another_person, test_bank_account, test_another_bank_account
):
    test_amount = "100"
    args = [test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_user_with_several_accounts(
    update_with_elected_mock, context_mock, test_bank_accounts, test_another_person, test_another_bank_account
):
    test_amount = "100"
    args = [test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=BANK_TRANSACTION_SEVERAL_SENDER_ACCOUNTS_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_unregistered_person(update_mock_from_unknown_user, context_mock):
    test_amount = "100"
    args = [update_mock_from_unknown_user.effective_user.usernmae, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_mock_from_unknown_user, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_unknown_user.effective_chat.id, text=BANK_TRANSACTION_NOT_REGISTER_SENDER_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_with_empty_accounts(update_mock_from_user_without_phone_number, context_mock):
    test_amount = "100"
    args = [update_mock_from_user_without_phone_number.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_mock_from_user_without_phone_number, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_user_without_phone_number.effective_chat.id, text=NO_BANK_ACCOUNT
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_to_person_with_several_accounts(
    update_mock_from_another_person_with_elected,
    test_another_bank_account,
    context_mock,
    test_person,
    test_bank_accounts,
):
    test_amount = "100"
    args = [test_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_mock_from_another_person_with_elected, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_another_person_with_elected.effective_chat.id,
        text=BANK_TRANSACTION_SEVERAL_RECEIVER_ACCOUNTS_MESSAGE,
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_with_incorrect_sum(
    update_with_elected_mock, context_mock, test_another_person, test_another_bank_account
):
    test_amount = "10q0"
    args = [test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=BANK_TRANSACTION_INCORRECT_SUM_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_card_number_receiver(
    update_with_elected_mock, context_mock, test_bank_account, test_another_bank_account
):
    test_amount = "100"
    args = [test_another_bank_account.number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_account_number_receiver(
    update_with_elected_mock, context_mock, test_bank_card, test_bank_account, test_another_bank_account
):
    test_amount = "100"
    args = [test_another_bank_account.number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_incorrect_card_number_receiver(
    update_with_elected_mock, context_mock, test_bank_account, test_another_bank_account
):
    test_amount = "100"
    incorrect_card_number = "1234*********111"
    args = [incorrect_card_number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=BANK_TRANSACTION_HELP_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_by_incorrect_account_number_receiver(update_with_elected_mock, context_mock, test_bank_account):
    test_amount = "100"
    incorrect_account_number = "1234*********1114444"
    args = [incorrect_account_number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=BANK_TRANSACTION_HELP_MESSAGE
    )


##############################################################################################
# Тесты для команды do_transaction с 3 аргументами
##############################################################################################


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_card_to_card(
    update_with_elected_mock,
    context_mock,
    test_bank_account,
    test_bank_card,
    test_another_person,
    test_another_bank_card,
    test_another_bank_account,
):
    test_amount = "100"
    args = [test_bank_card.card_number, test_another_bank_card.card_number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_card_to_account(
    update_with_elected_mock,
    context_mock,
    test_bank_account,
    test_bank_card,
    test_another_person,
    test_another_bank_account,
):
    test_amount = "100"
    args = [test_bank_card.card_number, test_another_bank_account.number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_account_to_card(
    update_with_elected_mock,
    context_mock,
    test_bank_account,
    test_another_person,
    test_another_bank_card,
    test_another_bank_account,
):
    test_amount = "100"
    args = [test_bank_account.number, test_another_bank_card.card_number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_account_to_account(
    update_with_elected_mock, context_mock, test_bank_account, test_another_person, test_another_bank_account
):
    test_amount = "100"
    args = [test_bank_account.number, test_another_bank_account.number, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_account_to_username(
    update_with_elected_mock, context_mock, test_bank_account, test_another_person, test_another_bank_account
):
    test_amount = "100"
    args = [test_bank_account.number, test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_card_to_username(
    update_with_elected_mock,
    context_mock,
    test_bank_card,
    test_bank_account,
    test_another_person,
    test_another_bank_account,
):
    test_amount = "100"
    args = [test_bank_card.card_number, test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
        f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{test_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{test_another_bank_account.number}.\n"
        f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{test_amount}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_do_transaction_from_card_to_person_without_account(
    update_with_elected_mock, context_mock, test_bank_card, test_bank_account, test_another_person
):
    test_amount = "100"
    args = [test_bank_card.card_number, test_another_person.telegram_username, test_amount]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.do_transaction(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=BANK_TRANSACTION_NOT_RECEIVER_ACCOUNT_MESSAGE
    )
