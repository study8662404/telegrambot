import pytest

from app.internal.persons.presentation.messages import (
    EXCLAMATION_MARK,
    HELLO,
    HELP_MESSAGE,
    INCORRECT_PHONE_FORMAT,
    INPUT_PHONE,
    NOT_AVAILABLE_COMMAND,
    PHONE,
    SAVE_PHONE,
    START_CREATED,
    START_HAS_PHONE,
    START_NO_PHONE,
    THANK,
    UNKNOWN_USER,
    USER_NAME,
)
from tests.integration.conftest import set_args_to_mock, test_person_handler


@pytest.mark.integration
@pytest.mark.django_db
def test_help_bot(update_mock, context_mock):
    test_person_handler.help_bot(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(chat_id=update_mock.effective_chat.id, text=HELP_MESSAGE)


@pytest.mark.integration
@pytest.mark.django_db
def test_start_with_known_user(update_mock, context_mock):
    test_person_handler.start(update_mock, context_mock)
    assert context_mock.bot.send_photo.call_args.kwargs["chat_id"] == update_mock.effective_chat.id
    assert (
        context_mock.bot.send_photo.call_args.kwargs["caption"]
        == f"{HELLO}{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{START_HAS_PHONE}"
    )
    # Приведенный ниже вариант не работает в связи с тем, что происходит открытие и чтение файла.
    # Содержимое экземпляров одинаковое, но сами экземпляры разные
    #   context_mock.bot.send_photo.assert_called_once_with(
    #     chat_id=update_mock.effective_chat.id,
    #     photo=open(path_to_picture, "rb"))
    #     caption=f"{HELLO}{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{START_HAS_PHONE}")


@pytest.mark.integration
@pytest.mark.django_db
def test_start_with_unknown_user(update_mock_from_unknown_user, context_mock):
    test_person_handler.start(update_mock_from_unknown_user, context_mock)
    assert context_mock.bot.send_photo.call_args.kwargs["chat_id"] == update_mock_from_unknown_user.effective_chat.id
    assert (
        context_mock.bot.send_photo.call_args.kwargs["caption"]
        == f"{HELLO}{update_mock_from_unknown_user.effective_user.first_name}{EXCLAMATION_MARK}{START_CREATED}"
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_start_with_user_without_phone(update_mock_from_user_without_phone_number, context_mock):
    test_person_handler.start(update_mock_from_user_without_phone_number, context_mock)
    assert (
        context_mock.bot.send_photo.call_args.kwargs["chat_id"]
        == update_mock_from_user_without_phone_number.effective_chat.id
    )
    assert (
        context_mock.bot.send_photo.call_args.kwargs["caption"]
        == f"{HELLO}{update_mock_from_user_without_phone_number.effective_user.first_name}{EXCLAMATION_MARK}{START_NO_PHONE}"
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone_with_not_empty_args(update_mock, context_mock):
    args = ["+79658882888"]
    set_args_to_mock(args, context_mock)
    test_person_handler.set_phone(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{THANK}{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{SAVE_PHONE}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone_with_empty_args(update_mock, context_mock):
    test_person_handler.set_phone(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=INCORRECT_PHONE_FORMAT
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone_with_incorrect_args(update_mock, context_mock):
    args = ["65787964"]
    set_args_to_mock(args, context_mock)
    test_person_handler.set_phone(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=INCORRECT_PHONE_FORMAT
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone_with_unknown_user(update_mock_from_unknown_user, context_mock):
    args = ["+79196552644"]
    set_args_to_mock(args, context_mock)
    test_person_handler.set_phone(update_mock_from_unknown_user, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_unknown_user.effective_chat.id,
        text=f"{update_mock_from_unknown_user.effective_user.first_name}{EXCLAMATION_MARK}{UNKNOWN_USER}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_me_with_unknown_user(update_mock_from_unknown_user, context_mock):
    test_person_handler.me(update_mock_from_unknown_user, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_unknown_user.effective_chat.id, text=f"{NOT_AVAILABLE_COMMAND}{UNKNOWN_USER}"
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_me_with_known_user(update_mock, context_mock):
    test_person_handler.me(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}!\n{USER_NAME}{update_mock.effective_user.username}, \n{PHONE}{update_mock.effective_user.phone_number}.",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_me_with_user_without_phone(update_mock_from_user_without_phone_number, context_mock):
    test_person_handler.me(update_mock_from_user_without_phone_number, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_user_without_phone_number.effective_chat.id,
        text=f"{NOT_AVAILABLE_COMMAND}{INPUT_PHONE}",
    )
