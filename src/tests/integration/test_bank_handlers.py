import pytest
from _decimal import Decimal

from app.internal.bank_accounts.data.models import BANK_NUMBER_VALIDATOR_MESSAGE
from app.internal.bank_accounts.presentation.messages import (
    BANK_ACCOUNT_AMOUNT,
    BANK_ACCOUNT_NUMBER,
    BANK_CARD_AMOUNT,
    EXCLAMATION_MARK,
    LINE_BREAK,
    NO_BANK_ACCOUNT,
    RUB,
)
from app.internal.bank_cards.data.models import CARD_NUMBER_VALIDATOR_MESSAGE
from app.internal.bank_cards.presentation.messages import BANK_CARD_NUMBER, EMPTY_BANK_CARD_NUMBER
from app.internal.bank_transactions.presentation.messages import (
    ACCOUNT_STATEMENT_HELP,
    BANK_ACCOUNT_NOT_LINKED,
    BANK_CARD_NOT_LINKED,
    CARD_STATEMENT_HELP,
    INCORRECT_ACCOUNT_FORMAT,
    INCORRECT_CARD_FORMAT,
    NO_TRANSACTION_INFORMATION,
)
from tests.integration.conftest import (
    set_args_to_mock,
    test_bank_account_handler,
    test_bank_card_handler,
    test_bank_transaction_handler,
)


@pytest.mark.integration
@pytest.mark.django_db
def test_account_balance_by_invalid_number(update_mock, context_mock):
    args = ["invalid_account_number_123"]
    set_args_to_mock(args, context_mock)
    test_bank_account_handler.account_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{BANK_NUMBER_VALIDATOR_MESSAGE}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_balance_by_unknown_number(update_mock, context_mock):
    account_number = "00000000000000000000"
    args = [account_number]
    set_args_to_mock(args, context_mock)
    test_bank_account_handler.account_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{BANK_ACCOUNT_NUMBER}{account_number}{BANK_ACCOUNT_NOT_LINKED}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_balance_by_number(update_mock, context_mock, test_bank_account):
    args = [test_bank_account.number]
    set_args_to_mock(args, context_mock)
    test_bank_account_handler.account_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{BANK_ACCOUNT_NUMBER}{test_bank_account.number}."
        f"{BANK_CARD_AMOUNT}{test_bank_account.amount} {RUB}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_balance_by_user_without_accounts(update_mock_from_user_without_phone_number, context_mock):
    test_bank_account_handler.account_balance(update_mock_from_user_without_phone_number, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_user_without_phone_number.effective_chat.id,
        text=f"{update_mock_from_user_without_phone_number.effective_user.first_name}{EXCLAMATION_MARK}{NO_BANK_ACCOUNT}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_balance_by_user(update_mock, context_mock, test_bank_accounts):
    test_bank_account_handler.account_balance(update_mock, context_mock)
    message_list = [f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}"]
    for account in test_bank_accounts:
        message_list.append(f"{BANK_ACCOUNT_NUMBER}{account.number}. {BANK_CARD_AMOUNT}{account.amount}{RUB}")
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=LINE_BREAK.join(message_list)
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_balance_by_invalid_number(update_mock, context_mock):
    args = ["invalid_card_number_123!"]
    set_args_to_mock(args, context_mock)
    test_bank_card_handler.card_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{CARD_NUMBER_VALIDATOR_MESSAGE}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_balance_by_empty_number(update_mock, context_mock):
    test_bank_card_handler.card_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{EMPTY_BANK_CARD_NUMBER}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_balance_with_unlinked_card_number(update_mock_from_user_without_phone_number, context_mock):
    card_number = "0000000000000000"
    args = [card_number]
    set_args_to_mock(args, context_mock)
    test_bank_card_handler.card_balance(update_mock_from_user_without_phone_number, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_from_user_without_phone_number.effective_chat.id,
        text=f"{update_mock_from_user_without_phone_number.effective_user.first_name}{EXCLAMATION_MARK}{BANK_CARD_NUMBER}{card_number}.{BANK_CARD_NOT_LINKED}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_balance(update_mock, context_mock, test_bank_card, test_bank_account):
    args = [test_bank_card.card_number]
    set_args_to_mock(args, context_mock)
    test_bank_card_handler.card_balance(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"{update_mock.effective_user.first_name}{EXCLAMATION_MARK}{BANK_CARD_NUMBER}"
        f"{test_bank_card.card_number}.{BANK_ACCOUNT_NUMBER}{test_bank_account.number}{BANK_ACCOUNT_AMOUNT}"
        f"{test_bank_account.amount}{RUB}",
    )


#######################################################################################################
# new bank commands: card_statement, account_statement, bank_interaction
#######################################################################################################
@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement(
    update_mock, context_mock, test_bank_account, test_bank_transaction, test_another_person, test_another_bank_account
):
    args = [test_bank_account.number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"Информация о транзакциях:\n\nПолучатель: {test_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_bank_transaction.recipient.number},"
        f" сумма: {test_bank_transaction.amount},"
        f" время совершения транзакции: {test_bank_transaction.created_at}.\n",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement_with_several_transactions(
    update_mock,
    context_mock,
    test_bank_account,
    test_bank_transaction,
    test_another_person,
    test_another_bank_account,
    test_second_bank_transaction,
    test_second_bank_account,
):
    args = [test_bank_account.number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"Информация о транзакциях:\n"
        f"\nПолучатель: {test_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_bank_transaction.recipient.number},"
        f" сумма: {test_bank_transaction.amount},"
        f" время совершения транзакции: {test_bank_transaction.created_at}.\n"
        f"\nПолучатель: {test_second_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_second_bank_transaction.recipient.number},"
        f" сумма: {test_second_bank_transaction.amount},"
        f" время совершения транзакции: {test_second_bank_transaction.created_at}.\n",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement(
    update_mock,
    context_mock,
    test_bank_account,
    test_bank_transaction,
    test_another_person,
    test_another_bank_account,
    test_bank_card,
):
    args = [test_bank_card.card_number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"Информация о транзакциях:\n\nПолучатель: {test_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_bank_transaction.recipient.number},"
        f" сумма: {test_bank_transaction.amount},"
        f" время совершения транзакции: {test_bank_transaction.created_at}.\n",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement_with_several_transactions(
    update_mock,
    context_mock,
    test_bank_account,
    test_bank_transaction,
    test_another_person,
    test_another_bank_account,
    test_second_bank_transaction,
    test_second_bank_account,
    test_bank_card,
):
    args = [test_bank_card.card_number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id,
        text=f"Информация о транзакциях:\n"
        f"\nПолучатель: {test_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_bank_transaction.recipient.number},"
        f" сумма: {test_bank_transaction.amount},"
        f" время совершения транзакции: {test_bank_transaction.created_at}.\n"
        f"\nПолучатель: {test_second_bank_transaction.recipient.account_owner.telegram_username},"
        f" номер счёта получателя: {test_second_bank_transaction.recipient.number},"
        f" сумма: {test_second_bank_transaction.amount},"
        f" время совершения транзакции: {test_second_bank_transaction.created_at}.\n",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_bank_interaction(
    update_mock,
    context_mock,
    test_person,
    test_another_person,
    test_bank_account,
    test_another_bank_account,
    test_bank_transaction,
):
    test_bank_transaction_handler.bank_interaction(update_mock, context_mock)
    expected_message = (
        f"Список пользователей, с которыми вы участвовали в транзакциях:\n\n{test_another_person.telegram_username}"
    )
    context_mock.bot.send_message.assert_called_once_with(chat_id=update_mock.effective_chat.id, text=expected_message)


@pytest.mark.integration
@pytest.mark.django_db
def test_bank_interaction_with_several_transactions(
    update_mock,
    context_mock,
    test_bank_transaction,
    test_second_bank_transaction,
    test_person,
    test_another_person,
    test_bank_account,
    test_second_bank_account,
    test_another_bank_account,
):
    test_bank_transaction_handler.bank_interaction(update_mock, context_mock)
    assert (
        test_person.telegram_username in context_mock.bot.send_message.call_args.kwargs["text"]
        and test_another_person.telegram_username in context_mock.bot.send_message.call_args.kwargs["text"]
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_bank_interaction_only_with_test_person(
    update_mock, context_mock, test_second_bank_transaction, test_person, test_bank_account, test_second_bank_account
):
    test_bank_transaction_handler.bank_interaction(update_mock, context_mock)
    expected_message = (
        f"Список пользователей, с которыми вы участвовали в транзакциях:\n" f"\n{test_person.telegram_username}"
    )
    context_mock.bot.send_message.assert_called_once_with(chat_id=update_mock.effective_chat.id, text=expected_message)


@pytest.mark.integration
@pytest.mark.django_db
def test_bank_interaction_only_with_test_another_person(
    update_mock,
    context_mock,
    test_another_bank_transaction,
    test_person,
    test_bank_account,
    test_another_person,
    test_another_bank_account,
):
    test_bank_transaction_handler.bank_interaction(update_mock, context_mock)
    expected_message = (
        f"Список пользователей, с которыми вы участвовали в транзакциях:\n" f"\n{test_another_person.telegram_username}"
    )
    context_mock.bot.send_message.assert_called_once_with(chat_id=update_mock.effective_chat.id, text=expected_message)


@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement_empty_args(update_mock, context_mock):
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ACCOUNT_STATEMENT_HELP
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement_with_several_args(update_mock, context_mock):
    args = ["arg1", "arg2"]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ACCOUNT_STATEMENT_HELP
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement_with_incorrect_account_number(update_mock, context_mock):
    args = ["1234567890*******111"]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=INCORRECT_ACCOUNT_FORMAT
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_account_statement_with_no_transaction_info(update_mock, context_mock, test_bank_account):
    args = [test_bank_account.number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.account_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=NO_TRANSACTION_INFORMATION
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement_empty_args(update_mock, context_mock):
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=CARD_STATEMENT_HELP
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement_with_several_args(update_mock, context_mock):
    args = ["arg1", "arg2"]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=CARD_STATEMENT_HELP
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement_with_incorrect_card_number(update_mock, context_mock):
    args = ["127890*******111"]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=INCORRECT_CARD_FORMAT
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_card_statement_with_no_transaction_info(update_mock, context_mock, test_bank_card, test_bank_account):
    args = [test_bank_card.card_number]
    set_args_to_mock(args, context_mock)
    test_bank_transaction_handler.card_statement(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=NO_TRANSACTION_INFORMATION
    )
