import pytest

from app.internal.persons.presentation.messages import (
    ADD_ELECTED_HELP_MESSAGE,
    ADD_ELECTED_MESSAGE,
    ADD_ELECTED_SUCCESS_MESSAGE,
    ADD_ELECTED_SUCCESS_YET_MESSAGE,
    ELECTED_UNREGISTERED_MESSAGE,
    GET_ELECTED_HELP_MESSAGE,
    GET_ELECTED_LIST_MESSAGE,
    REMOVE_ELECTED_HELP,
    REMOVE_ELECTED_NOT_EXIST,
    REMOVE_ELECTED_SUCCESS_MESSAGE,
)
from tests.integration.conftest import set_args_to_mock, test_person_handler


@pytest.mark.integration
@pytest.mark.django_db
def test_add_elected_incorrect_usage(update_mock, context_mock):
    test_person_handler.add_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ADD_ELECTED_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add_elected_invalid_username(update_mock, context_mock):
    args = ["#inc00rrect!!!"]
    set_args_to_mock(args, context_mock)
    test_person_handler.add_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ADD_ELECTED_HELP_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add_elected_with_unknown_elected_person(update_mock, context_mock):
    args = ["not_registered_user"]
    set_args_to_mock(args, context_mock)
    test_person_handler.add_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ELECTED_UNREGISTERED_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add_elected(update_mock, context_mock):
    args = [update_mock.effective_user.username]
    set_args_to_mock(args, context_mock)
    test_person_handler.add_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ADD_ELECTED_SUCCESS_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add_elected_twice(update_mock, context_mock):
    args = [update_mock.effective_user.username]
    set_args_to_mock(args, context_mock)
    test_person_handler.add_elected(update_mock, context_mock)
    test_person_handler.add_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_mock.effective_chat.id, text=ADD_ELECTED_SUCCESS_YET_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_get_elected(update_mock, context_mock):
    test_person_handler.get_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_mock.effective_chat.id, text=GET_ELECTED_HELP_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_get_elected_several(update_with_elected_mock, context_mock, test_another_person):
    args = [test_another_person.telegram_username]
    set_args_to_mock(args, context_mock)
    test_person_handler.get_elected(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_with_elected_mock.effective_chat.id,
        text=f"{GET_ELECTED_LIST_MESSAGE}{test_another_person.telegram_username}",
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_elected_incorrect_usage(update_with_elected_mock, context_mock):
    test_person_handler.remove_elected(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=REMOVE_ELECTED_HELP
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_elected_with_invalid_username(update_mock, context_mock):
    args = ["#inc00rrect!!!"]
    set_args_to_mock(args, context_mock)
    test_person_handler.remove_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ADD_ELECTED_HELP_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_elected_with_unknown_elected_person(update_mock, context_mock):
    args = ["not_registered_user"]
    set_args_to_mock(args, context_mock)
    test_person_handler.remove_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=ELECTED_UNREGISTERED_MESSAGE
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_elected_with_empty_elected_list(update_mock, context_mock, test_another_person):
    args = [test_another_person.telegram_username]
    set_args_to_mock(args, context_mock)
    test_person_handler.remove_elected(update_mock, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock.effective_chat.id, text=REMOVE_ELECTED_NOT_EXIST
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_elected(update_with_elected_mock, context_mock, test_another_person):
    args = [test_another_person.telegram_username]
    set_args_to_mock(args, context_mock)
    test_person_handler.remove_elected(update_with_elected_mock, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_with_elected_mock.effective_chat.id, text=REMOVE_ELECTED_SUCCESS_MESSAGE
    )
