from unittest.mock import MagicMock

import pytest

from app.internal.bank_accounts.app import get_bank_account_handlers
from app.internal.bank_cards.app import get_bank_card_handlers
from app.internal.bank_transactions.app import get_bank_transaction_handlers
from app.internal.persons.app import get_person_handlers

test_bank_card_handler = get_bank_card_handlers()
test_bank_account_handler = get_bank_account_handlers()
test_person_handler = get_person_handlers()
test_bank_transaction_handler = get_bank_transaction_handlers()


@pytest.fixture(scope="function")
def update_mock(test_person) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = test_person.telegram_id
    effective_user.username = test_person.telegram_username
    effective_user.first_name = test_person.firstname
    effective_user.phone_number = test_person.phone_number

    effective_chat = MagicMock()
    effective_chat.id = test_person.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def update_with_elected_mock(test_person_with_elected) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = test_person_with_elected.telegram_id
    effective_user.username = test_person_with_elected.telegram_username
    effective_user.first_name = test_person_with_elected.firstname
    effective_user.phone_number = test_person_with_elected.phone_number

    effective_chat = MagicMock()
    effective_chat.id = test_person_with_elected.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def update_mock_from_another_person(test_another_person) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = test_another_person.telegram_id
    effective_user.username = test_another_person.telegram_username
    effective_user.first_name = test_another_person.firstname
    effective_user.phone_number = test_another_person.phone_number

    effective_chat = MagicMock()
    effective_chat.id = test_another_person.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def update_mock_from_another_person_with_elected(test_another_person_with_elected) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = test_another_person_with_elected.telegram_id
    effective_user.username = test_another_person_with_elected.telegram_username
    effective_user.first_name = test_another_person_with_elected.firstname
    effective_user.phone_number = test_another_person_with_elected.phone_number

    effective_chat = MagicMock()
    effective_chat.id = test_another_person_with_elected.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def update_mock_from_unknown_user(unknown_person) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = unknown_person.telegram_id
    effective_user.username = unknown_person.telegram_username
    effective_user.first_name = unknown_person.firstname

    effective_chat = MagicMock()
    effective_chat.id = unknown_person.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def update_mock_from_user_without_phone_number(test_person_without_number) -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = test_person_without_number.telegram_id
    effective_user.username = test_person_without_number.telegram_username
    effective_user.first_name = test_person_without_number.firstname

    effective_chat = MagicMock()
    effective_chat.id = test_person_without_number.telegram_id

    update.effective_user = effective_user
    update.effective_chat = effective_chat
    return update


@pytest.fixture(scope="function")
def context_mock() -> MagicMock:
    context = MagicMock()
    return context


def set_args_to_mock(args, mock):
    mock.args = args
