import pytest
from django.test import Client


@pytest.mark.smoke
@pytest.mark.django_db
def test_get_bank_account_by_number(test_bank_account):
    url = f"/api/bank_account/get/{test_bank_account.number}"
    response = Client().get(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_get_bank_account_by_unknown_number():
    unknown_bank_account_number = "12312312312312312312"
    url = f"/api/bank_account/get/{unknown_bank_account_number}"
    response = Client().get(url)
    assert response.status_code == 404


@pytest.mark.django_db
def test_get_bank_account_by_card_number(test_bank_card, test_bank_account):
    url = f"/api/bank_account/get_by_card_number/{test_bank_card.card_number}"
    response = Client().get(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_get_bank_account_by_unknown_card_number():
    unknown_bank_card_number = "1231231231231231"
    url = f"/api/bank_account/get_by_card_number/{unknown_bank_card_number}"
    response = Client().get(url)
    assert response.status_code == 404
