import pytest
from django.contrib.auth import get_user_model


@pytest.mark.smoke
@pytest.mark.django_db
def test_db_access():
    user_model = get_user_model()
    user_model.objects.create_user("test1", "test1@test.com", "very_secret_test1_password")
    assert user_model.objects.count() == 1
