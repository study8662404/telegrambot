import pytest
from django.test import Client


@pytest.mark.smoke
@pytest.mark.django_db
def test_person_me(test_person):
    url = f"/api/person/me/{test_person.telegram_id}"
    response = Client().get(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_person_me_with_unknown():
    unknown_person_telegram_id = 123
    url = f"/api/person/me/{unknown_person_telegram_id}"
    response = Client().get(url)
    assert response.status_code == 404


@pytest.mark.smoke
@pytest.mark.django_db
def test_get_elected_list(test_person_with_elected):
    url = f"/api/person/get/{test_person_with_elected.telegram_id}/elected_persons_list"
    response = Client().get(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_delete_from_elected_list(test_person_with_elected, test_another_person):
    url = f"/api/person/delete/{test_person_with_elected.telegram_id}/elected_persons_list/{test_another_person.telegram_id}"
    response = Client().delete(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_delete_from_elected_list_without_elected_person(test_person, test_another_person):
    url = f"/api/person/delete/{test_person.telegram_id}/elected_persons_list/{test_another_person.telegram_id}"
    response = Client().delete(url)
    assert response.status_code == 400


@pytest.mark.smoke
@pytest.mark.django_db
def test_delete_from_elected_list_with_unknown_person(test_person):
    unknown_telegram_id = 1
    url = f"/api/person/delete/{unknown_telegram_id}/elected_persons_list/{test_person.telegram_id}"
    response = Client().delete(url)
    assert response.status_code == 400


@pytest.mark.smoke
@pytest.mark.django_db
def test_add_in_elected_list(test_person, test_another_person):
    url = f"/api/person/add/{test_person.telegram_id}/elected_persons_list/{test_another_person.telegram_id}"
    response = Client().post(url)
    assert response.status_code == 200


@pytest.mark.smoke
@pytest.mark.django_db
def test_add_in_elected_list_with_unknown_holder(test_another_person):
    unknown_telegram_id = 1
    url = f"/api/person/add/{unknown_telegram_id}/elected_persons_list/{test_another_person.telegram_id}"
    response = Client().post(url)
    assert response.status_code == 400


@pytest.mark.smoke
@pytest.mark.django_db
def test_add_in_elected_list_with_unknown_elected(test_person):
    unknown_telegram_id = 123
    url = f"/api/person/add/{test_person.telegram_id}/elected_persons_list/{unknown_telegram_id}"
    response = Client().post(url)
    assert response.status_code == 400
