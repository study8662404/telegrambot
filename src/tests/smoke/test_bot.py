import pytest

from app.bot.webhook_bot import WebhookBot


@pytest.mark.smoke
def test_webhook_service() -> None:
    test_bot = WebhookBot()
    assert test_bot.dispatcher.bot.get_webhook_info()
    # WebhookBot().process_new_update({})
