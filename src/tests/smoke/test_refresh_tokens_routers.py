import json
from http import HTTPStatus

import pytest
from django.test import Client

from app.internal.refresh_tokens.domain.services import TELEGRAM_ID_FIELD
from app.internal.refresh_tokens.presentation.handlers import LOGIN_FIELD, PASSWORD_FIELD
from tests.unit.conftest import refresh_token_service


@pytest.mark.smoke
@pytest.mark.django_db
def test_login(test_person_with_password):
    url = "/api/auth/login"
    data = {LOGIN_FIELD: test_person_with_password.telegram_username, PASSWORD_FIELD: "123456789"}
    response = Client().post(url, data=data)
    assert response.status_code == HTTPStatus.OK


@pytest.mark.smoke
@pytest.mark.django_db
def test_update_tokens_invalid(test_refresh_token_invalid):
    update_tokens_url = "/api/auth/update_tokens"
    update_tokens = Client().post(update_tokens_url, HTTP_refresh_token=test_refresh_token_invalid.jti)
    assert update_tokens.status_code == HTTPStatus.UNAUTHORIZED


@pytest.mark.smoke
@pytest.mark.django_db
def test_update_tokens(test_refresh_token):
    update_tokens_url = "/api/auth/update_tokens"
    update_tokens = Client().post(update_tokens_url, HTTP_refresh_token=test_refresh_token.jti)
    assert update_tokens.status_code == HTTPStatus.OK


@pytest.mark.smoke
@pytest.mark.django_db
def test_logout_with_unknown_person(test_refresh_token):
    url = "/api/auth/logout"
    logout_response = Client().post(url, HTTP_refresh_token=test_refresh_token.jti, HTTP_login="unknown_person")
    assert logout_response.status_code == HTTPStatus.UNAUTHORIZED


@pytest.mark.smoke
@pytest.mark.django_db
def test_logout_without_login(test_person_with_password, test_refresh_token_to_person_with_password):
    url = "/api/auth/logout"
    logout_response = Client().post(
        url,
        HTTP_refresh_token=test_refresh_token_to_person_with_password.jti,
        HTTP_login=test_person_with_password.telegram_username,
    )
    assert logout_response.status_code == HTTPStatus.OK


@pytest.mark.smoke
@pytest.mark.django_db
def test_logout(test_person_with_password):
    login_url = "/api/auth/login"
    logout_url = "/api/auth/logout"
    data = {LOGIN_FIELD: test_person_with_password.telegram_username, PASSWORD_FIELD: "123456789"}
    authentication_response = Client().post(login_url, data)
    assert authentication_response.status_code == HTTPStatus.OK
    refresh_token = json.loads(authentication_response.content)["refresh_token"]
    logout_response = Client().post(
        logout_url, HTTP_login=test_person_with_password.telegram_username, HTTP_refresh_token=refresh_token
    )
    assert logout_response.status_code == HTTPStatus.OK


@pytest.mark.smoke
@pytest.mark.django_db
def test_login_with_unknown_person():
    url = "/api/auth/login"
    data = {LOGIN_FIELD: "unknown", PASSWORD_FIELD: "123456789"}
    authentication_response = Client().post(url, data)
    assert authentication_response.status_code == HTTPStatus.UNAUTHORIZED


@pytest.mark.smoke
@pytest.mark.django_db
def test_login_with_incorrect_password(test_person_with_password):
    url = "/api/auth/login"
    data = {LOGIN_FIELD: test_person_with_password.telegram_username, PASSWORD_FIELD: "incorrect_password_123"}
    authentication_response = Client().post(url, data)
    assert authentication_response.status_code == HTTPStatus.FORBIDDEN


@pytest.mark.smoke
@pytest.mark.django_db
def test_login_with_refresh_token_compare(test_person_with_password):
    url = "/api/auth/login"
    data = {LOGIN_FIELD: test_person_with_password.telegram_username, PASSWORD_FIELD: "123456789"}
    authentication_response = Client().post(url, data=data)
    refresh_token = json.loads(authentication_response.content)["refresh_token"]

    decode_refresh_token = refresh_token_service.decode_token(refresh_token)
    assert decode_refresh_token[TELEGRAM_ID_FIELD] == test_person_with_password.telegram_id
