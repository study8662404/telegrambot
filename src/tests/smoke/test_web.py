import pytest
from django.test import Client


@pytest.mark.smoke
def test_web():
    resp = Client().get("/admin/")
    assert resp.status_code == 302
