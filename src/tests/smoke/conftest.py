import uuid
from datetime import datetime

import jwt
import phonenumbers
import pytest

from app.internal.persons.data.models import Person
from app.internal.refresh_tokens.app import get_refresh_token_service
from app.internal.refresh_tokens.data.models import RefreshToken
from app.internal.refresh_tokens.domain.services import EXPIRES_AT_FIELD, REFRESH_TOKEN_TTL, TELEGRAM_ID_FIELD
from config.settings import JWT_ENCODE_ALGORITHM, JWT_SECRET
from tests.unit.conftest import person_service


@pytest.fixture(scope="function")
def test_person_with_password(
    telegram_id=200, telegram_username="test200", firstname="Test200", phone_number="+79192000000"
):
    return Person.objects.create(
        telegram_id=telegram_id,
        telegram_username=telegram_username,
        firstname=firstname,
        phone_number=phone_number,
        password_hash=person_service.encrypt_password("123456789"),
    )


@pytest.fixture(scope="function")
def test_refresh_token(test_person):
    expires_at = (datetime.utcnow() + REFRESH_TOKEN_TTL).timestamp()
    refresh_token = jwt.encode(
        payload={TELEGRAM_ID_FIELD: test_person.telegram_id, EXPIRES_AT_FIELD: expires_at},
        key=JWT_SECRET,
        algorithm=JWT_ENCODE_ALGORITHM,
    )
    return RefreshToken.objects.create(jti=refresh_token, person=test_person)


@pytest.fixture(scope="function")
def test_refresh_token_invalid(test_person):
    expires_at = (datetime.utcnow() + REFRESH_TOKEN_TTL).timestamp()
    refresh_token = jwt.encode(
        payload={TELEGRAM_ID_FIELD: 404, EXPIRES_AT_FIELD: expires_at}, key=JWT_SECRET, algorithm=JWT_ENCODE_ALGORITHM
    )
    return RefreshToken.objects.create(jti=refresh_token, person=test_person)


@pytest.fixture(scope="function")
def test_refresh_token_to_person_with_password(test_person_with_password):
    expires_at = (datetime.utcnow() + REFRESH_TOKEN_TTL).timestamp()
    refresh_token = jwt.encode(
        payload={TELEGRAM_ID_FIELD: test_person_with_password.telegram_id, EXPIRES_AT_FIELD: expires_at},
        key=JWT_SECRET,
        algorithm=JWT_ENCODE_ALGORITHM,
    )
    return RefreshToken.objects.create(jti=refresh_token, person=test_person_with_password)


@pytest.fixture(scope="function")
def test_refresh_token_service():
    return get_refresh_token_service()
