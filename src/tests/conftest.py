import pytest
from _decimal import Decimal

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.bank_cards.data.models import BankCard
from app.internal.bank_transactions.data.models import BankTransaction
from app.internal.persons.data.models import Person


@pytest.fixture(scope="function")
def test_person(telegram_id=1, telegram_username="test1", firstname="Test1", phone_number="+79197777777"):
    return Person.objects.create(
        telegram_id=telegram_id,
        telegram_username=telegram_username,
        firstname=firstname,
        phone_number=phone_number,
    )


@pytest.fixture(scope="function")
def unknown_person(telegram_id=404, telegram_username="unknown_person", firstname="Unknown_person"):
    return Person(telegram_id=telegram_id, telegram_username=telegram_username, firstname=firstname)


@pytest.fixture(scope="function")
def test_person_without_number(telegram_id=3, telegram_username="test3", firstname="Test3"):
    return Person.objects.create(telegram_id=telegram_id, telegram_username=telegram_username, firstname=firstname)


@pytest.fixture(scope="function")
def test_bank_account(test_person: Person, number="1" * 20, amount=round(Decimal(1000.00), 2)):
    return BankAccount.objects.create(number=number, account_owner=test_person, amount=amount)


@pytest.fixture(scope="function")
def test_second_bank_account(test_person: Person, number="9" * 20, amount=round(Decimal(9000.00), 2)):
    return BankAccount.objects.create(number=number, account_owner=test_person, amount=amount)


@pytest.fixture(scope="function")
def test_bank_accounts(test_bank_account: BankAccount, test_second_bank_account: BankAccount):
    return [test_bank_account, test_second_bank_account]


@pytest.fixture(scope="function")
def test_bank_card(test_bank_account: BankAccount, card_number="1" * 16, valid_thru="01/24", cvc_cvv="111"):
    return BankCard.objects.create(
        card_number=card_number, valid_thru=valid_thru, cvc_cvv=cvc_cvv, bank_account=test_bank_account
    )


@pytest.fixture(scope="function")
def test_another_person(telegram_id=2, telegram_username="test2", firstname="Test2", phone_number="+79192222222"):
    return Person.objects.create(
        telegram_id=telegram_id,
        telegram_username=telegram_username,
        firstname=firstname,
        phone_number=phone_number,
    )


@pytest.fixture(scope="function")
def test_another_bank_account(test_another_person: Person, number="2" * 20, amount=round(Decimal(2000.0), 2)):
    return BankAccount.objects.create(number=number, account_owner=test_another_person, amount=amount)


@pytest.fixture(scope="function")
def test_another_bank_card(
    test_another_bank_account: BankAccount, card_number="2" * 16, valid_thru="02/24", cvc_cvv="222"
):
    return BankCard.objects.create(
        card_number=card_number, valid_thru=valid_thru, cvc_cvv=cvc_cvv, bank_account=test_another_bank_account
    )


@pytest.fixture(scope="function")
def test_amount(amount=100.0):
    return Decimal(amount)


@pytest.fixture(scope="function")
def test_person_with_elected(test_person: Person, test_another_person: Person):
    test_person.elected_persons.add(test_another_person)
    test_person.save()
    return test_person


@pytest.fixture(scope="function")
def test_another_person_with_elected(test_another_person: Person, test_person: Person):
    test_another_person.elected_persons.add(test_person)
    test_another_person.save()
    return test_another_person


@pytest.fixture(scope="function")
def test_bank_transaction(test_person, test_bank_account, test_another_person, test_another_bank_account):
    return BankTransaction.objects.create(
        sender=test_bank_account, recipient=test_another_bank_account, amount=round(Decimal(2000.0), 2)
    )


@pytest.fixture(scope="function")
def test_second_bank_transaction(test_person, test_bank_account, test_second_bank_account):
    return BankTransaction.objects.create(
        sender=test_bank_account, recipient=test_second_bank_account, amount=round(Decimal(2000.0), 2)
    )


@pytest.fixture(scope="function")
def test_another_bank_transaction(
    test_person, test_second_bank_account, test_another_person, test_another_bank_account
):
    return BankTransaction.objects.create(
        sender=test_another_bank_account, recipient=test_second_bank_account, amount=round(Decimal(2000.0), 2)
    )
