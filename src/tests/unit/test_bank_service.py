from typing import List

import pytest

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.bank_cards.data.models import BankCard
from tests.unit.conftest import bank_account_service


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bank_account_by_owner(test_bank_accounts: List[BankAccount]):
    actual_bank_account_out = bank_account_service.get_bank_account_by_owner(test_bank_accounts[0].account_owner)
    actual_bank_account_numbers = [account_out.number for account_out in actual_bank_account_out]
    expected_account_numbers = [test_account.number for test_account in test_bank_accounts]
    assert len(actual_bank_account_numbers) == len(expected_account_numbers)
    for number in expected_account_numbers:
        assert number in actual_bank_account_numbers


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bank_account_by_number(test_bank_account: BankAccount):
    actual = bank_account_service.get_bank_account_by_number(test_bank_account.number)
    assert actual.number == test_bank_account.number
    assert actual.amount == test_bank_account.amount
    assert actual.account_owner.telegram_id == test_bank_account.account_owner.telegram_id


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bank_account_by_card_number(test_bank_card: BankCard):
    actual = bank_account_service.get_bank_account_by_card_number(test_bank_card.card_number)
    assert actual.number == test_bank_card.bank_account.number
    assert actual.amount == test_bank_card.bank_account.amount
    assert actual.account_owner.telegram_id == test_bank_card.bank_account.account_owner.telegram_id
