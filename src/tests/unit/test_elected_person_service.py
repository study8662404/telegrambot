import pytest

from app.internal.persons.data.models import Person
from tests.unit.conftest import person_service


@pytest.mark.unit
@pytest.mark.django_db
def test_add_elected_person(test_person: Person, test_another_person: Person):
    created = person_service.add_elected_person(test_person, test_another_person)
    assert created


@pytest.mark.unit
@pytest.mark.django_db
def test_show_elected_persons(test_person_with_elected: Person, test_another_person: Person):
    actual = person_service.show_elected_persons(test_person_with_elected.telegram_id)
    assert actual == [test_another_person.telegram_username]


@pytest.mark.unit
@pytest.mark.django_db
def test_has_this_elected_person(test_person_with_elected: Person, test_another_person: Person):
    has_elected = person_service.has_this_elected_person(test_person_with_elected, test_another_person)
    assert has_elected


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_elected_person(test_person_with_elected: Person, test_another_person: Person):
    deleted = person_service.remove_elected_person(test_person_with_elected.telegram_id, test_another_person)
    assert deleted == 1
