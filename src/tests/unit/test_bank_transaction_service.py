import pytest
from _decimal import Decimal

from app.internal.bank_accounts.data.models import BankAccount
from tests.unit.conftest import bank_transaction_service


@pytest.mark.unit
@pytest.mark.django_db
def test_create_transaction(
    test_bank_account: BankAccount, test_another_bank_account: BankAccount, test_amount: Decimal
):
    actual = bank_transaction_service.create_transaction(test_bank_account, test_another_bank_account, test_amount)
    print(actual)
    assert actual.amount == test_amount
    assert actual.sender == test_bank_account.id
    assert actual.recipient == test_another_bank_account.id


@pytest.mark.unit
@pytest.mark.django_db
def test_get_transaction_info(
    test_bank_transaction, test_person, test_bank_account, test_another_person, test_another_bank_account
):
    sender_transaction_info, recipient_transaction_info = bank_transaction_service.get_transaction_info(
        test_bank_account
    )
    assert sender_transaction_info[0]["recipient__number"] == test_another_bank_account.number
    assert (
        sender_transaction_info[0]["recipient__account_owner__telegram_username"]
        == test_another_person.telegram_username
    )
    assert sender_transaction_info[0]["amount"] == test_bank_transaction.amount


@pytest.mark.unit
@pytest.mark.django_db
def test_get_transaction_info_with_several_transactions(
    test_bank_transaction, test_second_bank_transaction, test_person, test_bank_account, test_second_bank_account
):
    sender_transaction_info, recipient_transaction_info = bank_transaction_service.get_transaction_info(
        test_bank_account
    )
    for actual_transaction_info in sender_transaction_info:
        assert (
            actual_transaction_info["recipient__number"] == test_bank_transaction.recipient.number
            or actual_transaction_info["recipient__number"] == test_second_bank_transaction.recipient.number
        )
        assert (
            actual_transaction_info["amount"] == test_bank_transaction.amount
            or actual_transaction_info["amount"] == test_second_bank_transaction.amount
        )
        assert (
            actual_transaction_info["recipient__account_owner__telegram_username"]
            == test_bank_transaction.recipient.account_owner.telegram_username
            or actual_transaction_info["recipient__account_owner__telegram_username"]
            == test_second_bank_transaction.recipient.account_owner.telegram_username
        )


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bank_interaction_persons(
    test_person, test_another_person, test_bank_account, test_another_bank_account, test_bank_transaction
):
    usernames = bank_transaction_service.get_bank_interaction_persons(test_person)
    assert len(usernames) == 1
    assert test_another_person.telegram_username in usernames


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bank_interaction_persons_with_several_transactions(
    test_bank_transaction,
    test_second_bank_transaction,
    test_another_bank_transaction,
    test_person,
    test_another_person,
    test_bank_account,
    test_second_bank_account,
    test_another_bank_account,
):
    usernames = bank_transaction_service.get_bank_interaction_persons(test_person)
    assert len(usernames) == 2
    assert test_person.telegram_username in usernames
    assert test_another_person.telegram_username in usernames
