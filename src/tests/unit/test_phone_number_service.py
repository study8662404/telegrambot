import pytest

from app.internal.persons.data.models import Person
from tests.unit.conftest import person_service


@pytest.mark.unit
@pytest.mark.parametrize(
    "test_phone_number, expected",
    [("+79652550111", True), ("66614560", False), ("89195555555", True), ("9655555555", True), ("8919sd45601", False)],
)
def test_check_phone_number(test_phone_number: str, expected: bool):
    actual = person_service.check_phone_number(test_phone_number)
    valid = actual is not None
    assert valid == expected


@pytest.mark.unit
@pytest.mark.django_db
def test_phone_number_exists(test_person: Person):
    actual = person_service.phone_number_exists(test_person)
    assert actual == (test_person.phone_number != "")
