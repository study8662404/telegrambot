import pytest

from app.internal.persons.data.models import Person
from tests.unit.conftest import person_service


@pytest.mark.unit
@pytest.mark.django_db
def test_get_person_by_telegram_username(test_person: Person):
    actual = person_service.get_person_by_telegram_username(test_person.telegram_username)
    assert actual.telegram_id == test_person.telegram_id
    assert actual.telegram_username == test_person.telegram_username


@pytest.mark.unit
@pytest.mark.django_db
def test_get_person(test_person: Person):
    actual = person_service.get_person(test_person.telegram_id)
    assert actual.telegram_id == test_person.telegram_id
    assert actual.telegram_username == test_person.telegram_username


@pytest.mark.unit
@pytest.mark.django_db
def test_set_phone_number(test_person: Person):
    phone_number = "+79191111111"
    person_service.set_phone_number(test_person.telegram_id, phone_number)
    actual = person_service.get_person(test_person.telegram_id)
    assert actual.phone_number == phone_number


@pytest.mark.unit
@pytest.mark.django_db
def test_create_peron(test_person: Person):
    _, created = person_service.create_person(
        telegram_id=test_person.telegram_id,
        telegram_username=test_person.telegram_username,
        firstname=test_person.firstname,
    )
    assert not created
    _, new_created = person_service.create_person(
        telegram_id=2, telegram_username="test_username", firstname="test_name"
    )
    assert new_created
    new_created_person = person_service.get_person(2)
    assert new_created_person.telegram_username == "test_username"
    assert new_created_person.firstname == "test_name"


@pytest.mark.unit
@pytest.mark.django_db
def test_check_person_presence(test_person: Person):
    presence = person_service.check_person_presence(test_person.telegram_id)
    assert presence


@pytest.mark.unit
@pytest.mark.django_db
def test_get_full_information(test_person: Person):
    actual, no_phone_number = person_service.get_full_information(test_person.telegram_id)
    assert actual[0]["firstname"] == test_person.firstname
    assert actual[0]["telegram_username"] == test_person.telegram_username
    assert no_phone_number != person_service.phone_number_exists(test_person)
