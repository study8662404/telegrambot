from app.internal.bank_accounts.app import get_bank_account_service
from app.internal.bank_cards.app import get_bank_card_service
from app.internal.bank_transactions.app import get_bank_transaction_service
from app.internal.persons.app import get_person_service
from app.internal.refresh_tokens.app import get_refresh_token_service

bank_card_service = get_bank_card_service()
bank_account_service = get_bank_account_service()
person_service = get_person_service()
bank_transaction_service = get_bank_transaction_service()
refresh_token_service = get_refresh_token_service()
