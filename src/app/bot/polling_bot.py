from telegram.ext import Dispatcher, Updater

from app.bot.handlers import bot_bank_handlers, bot_person_handlers
from config.settings import TELEGRAM_BOT_TOKEN


class PollingBot:
    """
    Класс сущности Telegram Бота
    """

    # Массив, содержащий все обработчики, которые есть у бота
    all_bot_handlers = [bot_person_handlers, bot_bank_handlers]

    def run(self) -> None:
        """
        Функция запуска бота.
        :return: Отсутствуют выходные значения.
        """
        updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)  # извлекает обновления для бота и помещает их в
        # очередь для обработки
        dispatcher = updater.dispatcher  # отправляет все виды обновлений своим зарегистрированным обработчикам
        self.add_handlers(dispatcher)
        updater.start_polling()  # запускает опрос обновлений из Telegram
        updater.idle()

    def add_handlers(self, dispatcher: Dispatcher) -> None:
        """
        Функция, которая добавляет обработчики в dispatcher.
        :param dispatcher: Сущность, которая отправляет все виды обновлений своим зарегистрированным обработчикам.
        :return: Отсутствуют выходные значения.
        """
        for handlers in self.all_bot_handlers:
            for handler in handlers:
                dispatcher.add_handler(handler)
