from telegram import Bot, Update
from telegram.ext import Dispatcher, Updater

from app.bot.handlers import bot_bank_handlers, bot_person_handlers
from config.settings import BOT_DOCKER_PORT, DOMAIN, TELEGRAM_BOT_TOKEN


class WebhookBot:
    # Массив, содержащий все обработчики, которые есть у бота
    all_bot_handlers = [bot_person_handlers, bot_bank_handlers]

    def __init__(self):
        self.bot = Bot(TELEGRAM_BOT_TOKEN)
        self.updater = Updater(bot=self.bot)  # update_queue ---  это синхронизированная очередь,
        # которая будет содержать обновления.
        self.dispatcher = self.updater.dispatcher
        self.add_handlers(self.dispatcher)

    def add_handlers(self, dispatcher: Dispatcher) -> None:
        """
        Функция, которая добавляет обработчики в dispatcher.
        :param dispatcher: Сущность, которая отправляет все виды обновлений своим зарегистрированным обработчикам.
        :return: Отсутствуют выходные значения.
        """
        for handlers in self.all_bot_handlers:
            for handler in handlers:
                dispatcher.add_handler(handler)

    def process_new_update(self, json_data: dict) -> None:
        """
        Метод, который конвертирует полученные данные из JSON в объект Update телеграмма
        и отдаёт обработчику обновлений
        :param json_data: данные в формате JSON;
        :return: отсутствуют выходные значения.
        """
        update = Update.de_json(json_data, self.bot)  # Преобразует данные JSON в объект Telegram:
        # Принимает в качестве аргумента данные в формате JSON и бота, ассоциированного с объектом.
        # Возвращает Telegram object.
        self.dispatcher.process_update(update)  # Обрабатывает одно обновление

    def run(self):
        """
        Запуск Telegram-бота через Webhooks. В данном режиме сервер Telegram отправляет обновления
        боту после поступления нового.
        """
        self.updater.start_webhook(
            listen="0.0.0.0",
            port=BOT_DOCKER_PORT,
            url_path=TELEGRAM_BOT_TOKEN,
            webhook_url=f"https://{DOMAIN}/{TELEGRAM_BOT_TOKEN}",
        )
        self.updater.idle()
