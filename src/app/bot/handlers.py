"""
Массив, в котором хранятся обработчики, взаимодействующие с банковскими картами и счетами пользователя
"""
from telegram.ext import CommandHandler, Filters, MessageHandler

from app.bot.commands import BankCommands, PersonCommands
from app.internal.bank_accounts.app import get_bank_account_handlers
from app.internal.bank_cards.app import get_bank_card_handlers
from app.internal.bank_transactions.app import get_bank_transaction_handlers
from app.internal.persons.app import get_person_handlers

bank_card_handler = get_bank_card_handlers()
bank_account_handler = get_bank_account_handlers()
person_handler = get_person_handlers()
bank_transaction_handler = get_bank_transaction_handlers()


bot_bank_handlers = [
    CommandHandler(BankCommands.CARD_BALANCE_COMMAND.value, bank_card_handler.card_balance),
    CommandHandler(BankCommands.ACCOUNT_BALANCE_COMMAND.value, bank_account_handler.account_balance),
    CommandHandler(BankCommands.DO_TRANSACTION_COMMAND.value, bank_transaction_handler.do_transaction),
    MessageHandler(
        Filters.photo, bank_transaction_handler.do_transaction_with_postcard_handler
    ),  # для возможности обрабатывать команды транзакции с
    # прикрепленными открытками (изображениями)
    CommandHandler(BankCommands.ACCOUNT_STATEMENT_COMMAND.value, bank_transaction_handler.account_statement),
    CommandHandler(BankCommands.CARD_STATEMENT_COMMAND.value, bank_transaction_handler.card_statement),
    CommandHandler(BankCommands.BANK_INTERACTION_COMMAND.value, bank_transaction_handler.bank_interaction),
    CommandHandler(BankCommands.SHOW_NEW_TRANSACTIONS_COMMAND.value, bank_transaction_handler.show_new_transactions),
]

"""
Массив, в котором хранятся обработчики, взаимодействующие с профилем пользователя
--- изменение, отображение информации профиля пользователя, помощь пользователю.
Обработчик сообщений - это экземпляр, производный от базового класса telegram.ext.Handler,
который отвечает за передачу различных видов сообщений (текст, звук, встроенный запрос, нажатия кнопок и т. д.)
в соответствующую функцию обратного вызова.
Например, для того, чтобы бот отвечал на команду /start, нужно использовать обработчик
telegram.ext.CommandHandler, который сопоставляет ввод команды пользователя /start с
обратным вызовом с именем start.
"""
bot_person_handlers = [
    CommandHandler(PersonCommands.START_COMMAND.value, person_handler.start),
    CommandHandler(PersonCommands.SET_PHONE_COMMAND.value, person_handler.set_phone),
    CommandHandler(PersonCommands.ME_COMMAND.value, person_handler.me),
    CommandHandler(PersonCommands.HELP_COMMAND.value, person_handler.help_bot),
    CommandHandler(PersonCommands.ADD_ELECTED.value, person_handler.add_elected),
    CommandHandler(PersonCommands.GET_ELECTED.value, person_handler.get_elected),
    CommandHandler(PersonCommands.REMOVE_ELECTED.value, person_handler.remove_elected),
    # CommandHandler(PersonCommands.LOGIN.value, login),
]
