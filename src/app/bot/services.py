from telegram.ext import CallbackContext


def send_message(context: CallbackContext, chat_id: int, message: str) -> None:
    """
    Функция, которая отвечает за отправку сообщения
    :param context: контекст принятого сообщения.
    :param chat_id: Уникальный идентификатор чата, которому будет послано сообщение
    :param message: Текст отправляемого сообщения
    :return: Метод ничего не возвращает.
    """
    context.bot.send_message(
        chat_id=chat_id,
        text=message,
    )


def send_message_with_photo(context: CallbackContext, chat_id: int, picture: str, message: str) -> None:
    """
    Функция, которая отвечает за отправку сообщения, содержащего изображение
    :param context: контекст принятого сообщения.
    :param chat_id: Уникальный идентификатор чата, которому будет послано сообщение
    :param picture: Путь до изображения
    :param message: Текст отправляемого сообщения
    :return: Метод ничего не возвращает.
    """
    context.bot.send_photo(
        chat_id=chat_id,
        photo=open(picture, "rb"),
        caption=message,
    )
