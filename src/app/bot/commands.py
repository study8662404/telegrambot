from enum import Enum


class BankCommands(Enum):
    """
    Класс, содержащий команды боту, относящиеся к взаимодействию с банками, в виде перечисления.
    """

    CARD_BALANCE_COMMAND = "card_balance"
    ACCOUNT_BALANCE_COMMAND = "account_balance"
    DO_TRANSACTION_COMMAND = "do_transaction"
    ACCOUNT_STATEMENT_COMMAND = "account_statement"
    CARD_STATEMENT_COMMAND = "card_statement"
    BANK_INTERACTION_COMMAND = "bank_interaction"
    SHOW_NEW_TRANSACTIONS_COMMAND = "show_new_transactions"


class PersonCommands(Enum):
    """
    Класс, содержащий команды изменения и просмотра профиля пользователя, помощи пользователю в виде перечисления.
    """

    HELP_COMMAND = "help"
    START_COMMAND = "start"
    ME_COMMAND = "me"
    SET_PHONE_COMMAND = "set_phone"
    ADD_ELECTED = "add_elected"
    GET_ELECTED = "get_elected"
    REMOVE_ELECTED = "remove_elected"
    LOGIN = "login"
