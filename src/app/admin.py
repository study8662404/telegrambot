from django.contrib import admin

from app.internal.admins.presentation.admin import AdminUserAdmin
from app.internal.bank_accounts.presentation.admin import BankAccountAdmin
from app.internal.bank_cards.presentation.admin import BankCardAdmin
from app.internal.bank_transactions.presentation.admin import BankTransactionAdmin
from app.internal.persons.presentation.admin import PersonAdmin
from app.internal.postcards.presentation.admin import PostCardAdmin
from app.internal.refresh_tokens.presentation.admin import RefreshTokenAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
