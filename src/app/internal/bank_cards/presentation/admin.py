from django.contrib import admin

from app.internal.bank_cards.data.models import BankCard


@admin.register(BankCard)  # декоратор для регистрации классов ModelAdmin
class BankCardAdmin(admin.ModelAdmin):
    """
    Данный класс является представлением модели BankCard в интерфейсе администрирования.
    """

    fields = ["card_number", "valid_thru", "cvc_cvv", "bank_account"]
