from telegram import Update
from telegram.ext import CallbackContext

from app.bot.services import send_message
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_cards.domain.services import BankCardService


class BankCardHandlers:
    def __init__(self, bank_card_service: BankCardService, bank_account_service: BankAccountService):
        self.bank_card_service = bank_card_service
        self.bank_account_service = bank_account_service

    def card_balance(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'card_balance'. Вывод количества средств на банковской карте пользователя.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        indicate_card_number = len(context.args) == 1
        valid = False
        card_number = ""
        if indicate_card_number:
            card_number = context.args[0]
            valid = self.bank_card_service.check_bank_card_number_validity(card_number)
        message = self.bank_card_service.form_message_on_card_balance_command(
            self.bank_account_service, indicate_card_number, update.effective_user.first_name, valid, card_number
        )
        send_message(context, update.effective_chat.id, message)
