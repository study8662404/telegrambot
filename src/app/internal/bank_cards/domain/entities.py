from ninja.orm import create_schema

from app.internal.bank_accounts.domain.entities import BankAccountSchema
from app.internal.bank_cards.data.models import BankCard

BankCardSchema = create_schema(BankCard)


class BankCardIn(BankCardSchema):
    card_number: str
    bank_account: BankAccountSchema


class BankCardOut(BankCardSchema):
    card_number: str
    bank_account: BankAccountSchema
