from app.internal.bank_cards.data.models import BankCard


class IBankCardRepository:
    def get_bank_card_by_number(self, bank_card_id: str) -> BankCard:
        ...
