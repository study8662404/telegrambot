import re

from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_accounts.presentation.messages import BANK_ACCOUNT_AMOUNT, BANK_ACCOUNT_NUMBER
from app.internal.bank_cards.data.models import (
    CARD_NUMBER_VALIDATOR_MESSAGE,
    CARD_NUMBER_VALIDATOR_REGEX,
    MAX_CARD_NUMBER_LENGTH,
)
from app.internal.bank_cards.data.repositories import IBankCardRepository
from app.internal.bank_cards.domain.entities import BankCardOut
from app.internal.bank_cards.presentation.messages import (
    BANK_CARD_NOT_LINKED,
    BANK_CARD_NUMBER,
    EMPTY_BANK_CARD_NUMBER,
    EXCLAMATION_MARK,
    RUB,
)


class BankCardService:
    def __init__(self, bank_card_repo: IBankCardRepository):
        self._bank_card_repo = bank_card_repo

    def get_bank_card_by_number(self, card_number: str):
        bank_card = self._bank_card_repo.get_bank_card_by_number(card_number)
        return BankCardOut.from_orm(bank_card)

    def check_bank_card_number_validity(self, card_number: str) -> bool:
        """
        Проверка валидности номера банковской карты.
        :param card_number: Номер банковской карты
        return True, если номер карты валиден. Иначе False.
        """
        return (
            len(card_number) == MAX_CARD_NUMBER_LENGTH
            and re.search(CARD_NUMBER_VALIDATOR_REGEX, card_number) is not None
        )

    def form_message_on_card_balance_command(
        self,
        bank_account_service: BankAccountService,
        indicate_card_number: bool,
        firstname: str,
        valid: bool,
        card_number: str,
    ) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "card_balance".
        :param indicate_card_number: Логическая переменная, был ли указан в сообщении номер банковской карты;
        :param firstname: имя пользователя;
        :param valid: Логическая переменная, является ли номер банковской карты, переданный в сообщении, валидным
        :param card_number: номер банковской карты.
        :return: Возвращает сообщение пользователю в ответ на команду "card_balance".
        """
        message = ""
        if not indicate_card_number:
            message = f"{firstname}{EXCLAMATION_MARK}{EMPTY_BANK_CARD_NUMBER}"
        else:
            if not valid:
                message = f"{firstname}{EXCLAMATION_MARK}{CARD_NUMBER_VALIDATOR_MESSAGE}"
            else:
                bank_account = bank_account_service.get_bank_account_by_card_number(card_number)
                if bank_account is None:
                    message = f"{firstname}{EXCLAMATION_MARK}{BANK_CARD_NUMBER}{card_number}.{BANK_CARD_NOT_LINKED}"
                else:
                    message = (
                        f"{firstname}{EXCLAMATION_MARK}{BANK_CARD_NUMBER}{card_number}."
                        f"{BANK_ACCOUNT_NUMBER}{bank_account.number}"
                        f"{BANK_ACCOUNT_AMOUNT}{bank_account.amount}{RUB}"
                    )
        return message
