from app.internal.bank_accounts.app import get_bank_account_service
from app.internal.bank_cards.data.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_cards.presentation.handlers import BankCardHandlers


def get_bank_card_service():
    bank_card_repo = BankCardRepository()
    bank_card_service = BankCardService(bank_card_repo=bank_card_repo)
    return bank_card_service


def get_bank_card_handlers():
    bank_card_service = get_bank_card_service()
    bank_account_service = get_bank_account_service()
    bank_card_handler = BankCardHandlers(bank_card_service=bank_card_service, bank_account_service=bank_account_service)
    return bank_card_handler
