from app.internal.bank_cards.data.models import BankCard
from app.internal.bank_cards.domain.repo_interface import IBankCardRepository


class BankCardRepository(IBankCardRepository):
    def get_bank_card_by_number(self, bank_card_number: str) -> BankCard:
        return BankCard.objects.filter(card_number=bank_card_number).first()
