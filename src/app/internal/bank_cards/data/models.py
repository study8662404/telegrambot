from django.core.validators import RegexValidator
from django.db import models

from app.internal.bank_accounts.data.models import BankAccount

MAX_CARD_NUMBER_LENGTH = 16
CARD_NUMBER_VALIDATOR_REGEX = f"\\d{{{MAX_CARD_NUMBER_LENGTH}}}"
CARD_NUMBER_VALIDATOR_MESSAGE = (
    "Проверьте, пожалуйста, корректность введённого номера банковской карты! "
    "Номер карты должен состоять из 16 десятичных цифр, введённых без пробельных символов."
)

MAX_THRU_LENGTH = 5
CARD_THRU_VALIDATOR_REGEX = "(0[1-9]|1[0-2])/([0-9]{2})"
CARD_THRU_VALIDATOR_MESSAGE = (
    "Проверьте, пожалуйста, корректность введённого срока банковской карты! "
    "Данные должны быть введены в формате: месяц/год. Месяц имеет формат: от 01 до 12, "
    "год имеет формат: от 00 до 99."
)

MAX_CODE_LENGTH = 3
CODE_VALIDATOR_REGEX = "[0-9]{3}"
CODE_VALIDATOR_MESSAGE = (
    "Проверьте, пожалуйста, корректность введённого кода CVC/CVV банковской карты. "
    "Данный код имеет формат: от 000 до 999."
)

card_number_validator = RegexValidator(regex=CARD_NUMBER_VALIDATOR_REGEX, message=CARD_NUMBER_VALIDATOR_MESSAGE)

card_thru_validator = RegexValidator(regex=CARD_THRU_VALIDATOR_REGEX, message=CARD_THRU_VALIDATOR_MESSAGE)

code_validator = RegexValidator(regex=CODE_VALIDATOR_REGEX, message=CODE_VALIDATOR_MESSAGE)


class BankCard(models.Model):
    """
    Данный класс представляет собой модель банковской карты пользователя Telegram бота.
    Содержит поля и параметры хранения данных: номер банковской карты, срок действия карты,
    код с обратной стороны карты, банковский счёт, к которому привязана карта.
    Сопоставляется с одной таблицей базы данных.
    """

    card_number = models.CharField(
        max_length=MAX_CARD_NUMBER_LENGTH,
        validators=[card_number_validator],
        unique=True,
        verbose_name="Номер банковской карты",
    )
    valid_thru = models.CharField(
        max_length=MAX_THRU_LENGTH, validators=[card_thru_validator], verbose_name="Срок действия банковской карты"
    )
    cvc_cvv = models.CharField(
        max_length=MAX_CODE_LENGTH, validators=[code_validator], verbose_name="Код с обратной стороны карты"
    )
    bank_account = models.ForeignKey(
        BankAccount,  # Условие: карты должны привязываться к счетам
        on_delete=models.CASCADE,  # При удалении счёта происходит удаление карты
        verbose_name="Банковский счёт, к которому привязана карта",
    )
    objects = models.Manager()  # менеджер объектов

    def __str__(self):
        return f"card: {self.card_number}"

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели банковской карты.
        """

        verbose_name = "Банковская карта"  # Удобочитаемое имя для поля, единственное число
        verbose_name_plural = "Банковские карты"  # Удобочитаемое имя для поля, множественное число
