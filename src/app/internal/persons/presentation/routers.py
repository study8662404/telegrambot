from typing import List

from ninja import NinjaAPI, Router

from app.internal.broad.responses import ErrorResponse, NotFoundResponse, SuccessResponse
from app.internal.persons.domain.entities import PersonOut
from app.internal.persons.presentation.handlers import PersonHandlers


def get_person_router(person_handlers: PersonHandlers):
    router = Router()

    router.add_api_operation(
        "get/{int:holder_id}/elected_persons_list",
        ["GET"],
        person_handlers.get_elected_persons,
        response={200: List[str]},
    )

    router.add_api_operation(
        "add/{int:person_id}/elected_persons_list/{int:id_person_to_add}",
        ["POST"],
        person_handlers.add_person_to_elected_persons,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "delete/{int:person_id}/elected_persons_list/{int:id_to_delete}",
        ["DELETE"],
        person_handlers.remove_person_from_elected_persons,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "me/{int:person_id}",
        ["GET"],
        person_handlers.get_person_by_id,
        response={200: SuccessResponse, 404: NotFoundResponse},
    )
    return router


def add_person_router(api: NinjaAPI, person_handlers: PersonHandlers):
    persons_handler = get_person_router(person_handlers)
    api.add_router("person", persons_handler)
