from django.contrib import admin

from app.internal.persons.data.models import Person
from app.internal.persons.presentation.form import PersonForm


@admin.register(Person)  # декоратор для регистрации классов ModelAdmin
class PersonAdmin(admin.ModelAdmin):
    """
    Данный класс является представлением модели Person в интерфейсе администрирования.
    """

    fields = ["telegram_id", "telegram_username", "firstname", "phone_number", "password_hash"]
    readonly_fields = ("password_hash",)
    form = PersonForm
