import os
from typing import List

from ninja import Path
from telegram import Update
from telegram.ext import CallbackContext

from app.bot.services import send_message, send_message_with_photo
from app.internal.broad.responses import ErrorResponse, NotFoundResponse, SuccessResponse
from app.internal.persons.domain.services import PersonService
from app.internal.persons.presentation.messages import HELP_MESSAGE


class PersonHandlers:
    def __init__(self, person_service: PersonService):
        self._person_service = person_service

    def get_person_by_id(self, request, person_id: int = Path(...)):
        """
        Реализация команды эндпоинт/me.
        Возвращает информацию о пользователе в формате Json либо ошибку, если запрашиваемого
        пользователя нет в базе данных бота.
        :param request: Объект, содержащий метаданные о запросе.
        :param person_id: Идентификатор пользователя в Telegram.
        :return: Информация в формате Json:
        если пользователь с переданным telegram_id либо пользователе, если он существует в базе данных,
        либо сообщение об ошибке,
        """
        person = self._person_service.get_person(person_id)
        if person is None:
            return 404, NotFoundResponse(message="Пользователь не найден!")
        return 200, SuccessResponse(success=person)

    def add_person_to_elected_persons(self, request, person_id: int = Path(...), id_person_to_add: int = Path(...)):
        holder = self._person_service.get_person(person_id)
        if holder is None:
            return 400, ErrorResponse(message="Отсутствует пользователь с переданным идентификатором!")
        elected_person = self._person_service.get_person(id_person_to_add)
        if elected_person is None:
            return 400, ErrorResponse(message="Отсутствует избранный пользователь с переданным идентификатором!")
        success = self._person_service.add_elected_person(holder, elected_person)
        if not success:
            return 400, ErrorResponse(message="Ошибка при добавлении пользователя в список избранных!")
        return 200, SuccessResponse(success={"success": success})

    def remove_person_from_elected_persons(self, request, person_id: int = Path(...), id_to_delete: int = Path(...)):
        elected_person = self._person_service.get_person(id_to_delete)
        success = self._person_service.remove_elected_person(person_id, elected_person)
        if not success:
            return 400, ErrorResponse(message="Ошибка при удалении пользователя из списка избранных!")
        return 200, SuccessResponse(success={"success": success})

    def get_elected_persons(self, request, holder_id: int = Path(...)) -> List[str]:
        elected_persons = self._person_service.show_elected_persons(holder_id)
        return elected_persons

    def help_bot(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'help'.
        Посылает пользователю сообщение со вспомогательной информацией о сервисе бота
        --- доступных командах и правилах их использования.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        message = HELP_MESSAGE
        send_message(context, update.effective_chat.id, message)

    def start(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'start'.
        Запись информации о пользователе в базу данных --- уникальный идентификатор пользователя,
        имя пользователя в Telegram, имя пользователя.
        Посылает пользователю сообщение, содержащие картинку и приветствие, которое зависит от уровня заполненности
        профиля пользователя у бота.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        telegram_user = update.effective_user  # пользователь, который прислал обновление
        message = self._person_service.form_message_on_start_command(telegram_user)
        cwd = os.getcwd()  # Get the current working directory (cwd)
        root_proj_dir = "src"
        index_src = cwd.find(root_proj_dir)
        if index_src == -1:
            path_to_picture = f"{cwd}/{root_proj_dir}/resources/mini_label.png"
        else:
            path_to_picture = f"{cwd[:index_src + len(root_proj_dir)]}/resources/mini_label.png"
        send_message_with_photo(context, update.effective_chat.id, path_to_picture, message)

    def set_phone(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'set_phone'.
        Запись номера телефона пользователя в базу данных.
        Посылает пользователю сообщение о том, успешно ли прошла данная операция.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        telegram_user = update.effective_user  # пользователь, который прислал обновление
        phone_number = None if len(context.args) == 0 else context.args[0]  # номер телефона пользователя
        # передаётся первым аргументом команды /set_phone
        message = self._person_service.form_message_on_set_phone_command(
            telegram_user.first_name, telegram_user.id, phone_number
        )
        send_message(context, update.effective_chat.id, message)

    def me(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'me'.
        Выводит всю известную информацию о пользователе или сообщение, содержащее,
        что нужно сделать пользователю, чтобы эта команда стала доступна.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        telegram_user = update.effective_user  # пользователь, который прислал обновление
        message = self._person_service.form_message_on_me_command(telegram_user.id)
        send_message(context, update.effective_chat.id, message)

    # def login(self, update: Update, context: CallbackContext) -> None:
    #     message = ""
    #     send_password = len(context.args) == 1
    #     if not send_password:
    #         message = "Данная команда принимает в качестве аргумента пароль для входа в приложение."
    #     else:
    #         message = form_massage_on_login_command(update.effective_user.id, context.args[0])
    #     send_message(context, update.effective_chat.id, message)

    def add_elected(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'add_elected'.
        Добавление пользователя, переданного в качестве аргумента команды, в список избранных
        текущего пользователя. Если пользователь присутствует в списке избранных,
        то текущий пользователь может перечислять денежные средства на его карту/банковский аккаунт.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        incorrect_usage = len(context.args) != 1
        valid = False
        elected_telegram_username = ""
        if not incorrect_usage:
            elected_telegram_username = context.args[0]
            valid = self._person_service.validate_telegram_username(elected_telegram_username)
        message = self._person_service.form_message_on_add_elected_command(
            incorrect_usage, valid, elected_telegram_username, update.effective_user.id
        )
        send_message(context, update.effective_chat.id, message)

    def get_elected(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'get_elected'.
        Просмотр списка избранных пользователей, которым текущий пользователь, приславший сообщение боту,
        может перечислять денежные средства на карту/банковский счёт.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        message = self._person_service.form_message_on_get_elected_command(update.effective_user.id)
        send_message(context, update.effective_chat.id, message)

    def remove_elected(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'remove_elected'.
        Удаление пользователя, указанного в качестве аргумента команды,
        из списка избранного пользователя, который прислал сообщение боту.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        incorrect_usage = len(context.args) != 1
        valid = False
        elected_telegram_username = ""
        if not incorrect_usage:
            elected_telegram_username = context.args[0]
            valid = self._person_service.validate_telegram_username(elected_telegram_username)
        message = self._person_service.form_message_on_remove_elected_command(
            incorrect_usage, valid, update.effective_user.id, elected_telegram_username
        )
        send_message(context, update.effective_chat.id, message)
