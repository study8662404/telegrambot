from django import forms
from phonenumber_field.formfields import PhoneNumberField

from app.internal.persons.data.models import Person
from config.settings import REGION


class PersonForm(forms.ModelForm):
    """
    Класс, который используется для преобразования модели Person в форму Django.
    """

    class Meta:
        model = Person
        fields = (
            "telegram_id",
            "telegram_username",
            "firstname",
            "phone_number",
        )
        phone_numer = PhoneNumberField(region=REGION)  # валидация номера телефона согласно региону
