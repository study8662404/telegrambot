from typing import List, Optional

from app.internal.persons.data.models import Person
from app.internal.persons.domain.entities import PersonIn, PersonOut


class IPersonRepository:
    def add_elected_person(self, holder: PersonOut, elected_person: PersonOut) -> bool:
        ...

    def remove_elected_person(self, holder_telegram_id: int, elected_person: Person) -> bool:
        ...

    def show_elected_persons(self, holder_telegram_id: int) -> List[str]:
        ...

    def has_this_elected_person(self, holder: PersonIn, elected_person: PersonIn) -> bool:
        ...

    def create_person(self, telegram_id: int, telegram_username: str, firstname: str) -> (bool, bool):
        ...

    def check_person_presence(self, telegram_id: int) -> bool:
        ...

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        ...

    def get_person(self, telegram_id: int) -> Person:
        ...

    def get_full_information(self, telegram_id: int) -> (Optional[Person], bool):
        ...

    def get_person_by_telegram_username(self, telegram_username: str) -> Person | None:
        ...

    def check_password(self, telegram_id: int, password_hash: bytes) -> bool:
        ...
