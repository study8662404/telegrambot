from ninja.orm import create_schema

from app.internal.persons.data.models import Person

PersonSchema = create_schema(Person)


class PersonIn(PersonSchema):
    telegram_id: int
    telegram_username: str
    firstname: str
    # phone_number: str
    # elected_persons: PersonSchema


class PersonOut(PersonSchema):
    telegram_id: int
    telegram_username: str
    firstname: str
