import hashlib
from typing import List

import phonenumber_field
from phonenumbers import NumberParseException
from telegram import User

from app.internal.persons.data.repositories import IPersonRepository
from app.internal.persons.domain.entities import PersonIn, PersonOut
from app.internal.persons.presentation.messages import (
    ADD_ELECTED_HELP_MESSAGE,
    ADD_ELECTED_MESSAGE,
    ADD_ELECTED_SUCCESS_MESSAGE,
    ADD_ELECTED_SUCCESS_YET_MESSAGE,
    ELECTED_UNREGISTERED_MESSAGE,
    EXCLAMATION_MARK,
    GET_ELECTED_HELP_MESSAGE,
    GET_ELECTED_LIST_MESSAGE,
    HELLO,
    INCORRECT_PHONE_FORMAT,
    INPUT_PHONE,
    LINE_BREAK,
    NOT_AVAILABLE_COMMAND,
    PHONE,
    REMOVE_ELECTED_HELP,
    REMOVE_ELECTED_NOT_EXIST,
    REMOVE_ELECTED_SUCCESS_MESSAGE,
    SAVE_PHONE,
    START_CREATED,
    START_HAS_PHONE,
    START_NO_PHONE,
    THANK,
    UNKNOWN_USER,
    USER_NAME,
)
from config.settings import (
    PASSWORD_ENCODE_ALGORITHM,
    PASSWORD_ENCODING_NAME,
    PASSWORD_ITERATIONS_COUNT,
    PASSWORD_SALT,
    REGION,
)


class PersonService:
    def __init__(self, person_repo: IPersonRepository):
        self._person_repo = person_repo

    def create_person(self, telegram_id: int, telegram_username: str, firstname: str) -> (bool, bool):
        return self._person_repo.create_person(telegram_id, telegram_username, firstname)

    def check_person_presence(self, telegram_id: int) -> bool:
        return self._person_repo.check_person_presence(telegram_id)

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        self._person_repo.set_phone_number(telegram_id, phone_number)

    def get_person(self, telegram_id: int) -> PersonOut | None:
        person = self._person_repo.get_person(telegram_id)
        return PersonOut.from_orm(person) if person is not None else None

    def get_full_information(self, telegram_id: int) -> (PersonOut, bool):
        info, phone_number_exists = self._person_repo.get_full_information(telegram_id)
        return info, phone_number_exists

    def get_person_by_telegram_username(self, telegram_username: str) -> PersonOut | None:
        person = self._person_repo.get_person_by_telegram_username(telegram_username)
        return PersonOut.from_orm(person) if person is not None else None

    def check_password(self, telegram_id: int, password_hash: bytes) -> bool:
        return self._person_repo.check_password(telegram_id, password_hash)

    def add_elected_person(self, holder: PersonOut, elected_person: PersonOut) -> bool:
        return self._person_repo.add_elected_person(holder, elected_person)

    def remove_elected_person(self, holder_telegram_id: int, elected_person: PersonIn) -> bool:
        return self._person_repo.remove_elected_person(holder_telegram_id, elected_person)

    def show_elected_persons(self, holder_telegram_id: int) -> List[str]:
        return self._person_repo.show_elected_persons(holder_telegram_id)

    def has_this_elected_person(self, holder: PersonIn, elected_person: PersonIn) -> bool:
        return self._person_repo.has_this_elected_person(holder, elected_person)

    def validate_telegram_username(self, telegram_username: str) -> bool:
        is_valid_length = len(telegram_username) >= 5
        telegram_username_without_ = telegram_username.replace("_", "")
        return telegram_username_without_.isalnum() and is_valid_length

    def check_phone_number(self, phone_number: str) -> str | None:
        """
        Проверка на корректность введённого телефонного номера.
        :param phone_number: Телефонный номер пользователя в виде строки.
        :return: Телефонный номер пользователя как объект PhoneNumber, если телефон введён корректно.
        Иначе None.
        """
        try:
            number = phonenumber_field.phonenumber.PhoneNumber.from_string(phone_number, region=REGION)
            valid = phonenumber_field.phonenumber.PhoneNumber.is_valid(number)
            return phone_number if valid else None
        except NumberParseException:
            return None

    def phone_number_exists(self, person: PersonIn) -> bool:
        """
        Проверяет наличие номера телефона у пользователя
        :param person: Объект пользователя
        :return: True, если номер телефона есть у объекта пользователя. Иначе, False.
        """
        if person is None:
            return False
        return person.phone_number != ""

    def encrypt_password(self, password: str) -> bytes:
        """
        Шифрование пароля с помощью функции pbkdf2_hmac. Она принимает в качестве аргументов имя алгоритма,
        с помощью которого происходит хэширование; пароль и соль, которые интерпретируются как буферы байтов;
        количество итераций для алгоритма хэширования.
        """
        password_hash = hashlib.pbkdf2_hmac(
            PASSWORD_ENCODE_ALGORITHM,
            password.encode(PASSWORD_ENCODING_NAME),
            PASSWORD_SALT.encode(PASSWORD_ENCODING_NAME),
            int(PASSWORD_ITERATIONS_COUNT),
        )
        return password_hash

    def compare_passwords_hash(self, actual: bytes, expected: bytes) -> bool:
        """
        Сравнение паролей по хэшам.
        """
        return actual == expected

    def form_message_on_add_elected_command(
        self, incorrect_usage: bool, valid: bool, elected_telegram_username: str, effective_user_id: int
    ) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "add_elected".
        :param incorrect_usage: Логическая переменная, которая сигнализирует о некорректном использовании команды.
        :param valid: Логическая переменная, которая сигнализирует о том,
        является ли переданный аргумент корректным именем пользователя в Telegram.
        :param elected_telegram_username: Имя пользователя в Telegram, которого хотят добавить в список избранных;
        :param effective_user_id: Уникальный идентификатор пользователя, приславшего сообщение;
        :return: Возвращает сообщение пользователю в ответ на команду add_elected.
        """
        if incorrect_usage:
            message = ADD_ELECTED_MESSAGE
        else:
            if not valid:
                message = ADD_ELECTED_HELP_MESSAGE
            else:
                elected_person = self.get_person_by_telegram_username(elected_telegram_username)
                if elected_person is None:
                    message = ELECTED_UNREGISTERED_MESSAGE
                else:
                    holder = self.get_person(effective_user_id)
                    created = self.add_elected_person(holder, elected_person)
                    if created:
                        message = ADD_ELECTED_SUCCESS_MESSAGE
                    else:
                        message = ADD_ELECTED_SUCCESS_YET_MESSAGE
        return message

    def form_message_on_get_elected_command(self, effective_user_id: int) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "get_elected".
        :param effective_user_id: Уникальный идентификатор пользователя, приславшего сообщение;
        :return: Возвращает сообщение пользователю в ответ на команду get_elected.
        """
        elected_persons_username_list = self.show_elected_persons(effective_user_id)
        message = ""
        if len(elected_persons_username_list) == 0:
            message = GET_ELECTED_HELP_MESSAGE
        else:
            message_elected_persons = LINE_BREAK.join(elected_persons_username_list)
            message = f"{GET_ELECTED_LIST_MESSAGE}{message_elected_persons}"
        return message

    def form_message_on_remove_elected_command(
        self, incorrect_usage: bool, valid: bool, effective_user_id: int, elected_telegram_username: str
    ) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "remove_elected".
        :param incorrect_usage: Логическая переменная, которая сигнализирует о некорректном использовании команды.
        :param valid: Логическая переменная, которая сигнализирует о том,
        является ли переданный аргумент корректным именем пользователя в Telegram.
        :param elected_telegram_username: Имя пользователя в Telegram, которого хотят добавить в список избранных;
        :param effective_user_id: Уникальный идентификатор пользователя, приславшего сообщение;
        :return: Возвращает сообщение пользователю в ответ на команду remove_elected.
        """
        message = ""
        if incorrect_usage:
            message = REMOVE_ELECTED_HELP
        else:
            if not valid:
                message = ADD_ELECTED_HELP_MESSAGE
            else:
                elected_person = self.get_person_by_telegram_username(elected_telegram_username)
                if elected_person is None:
                    message = ELECTED_UNREGISTERED_MESSAGE
                else:
                    deleted_person_number = self.remove_elected_person(effective_user_id, elected_person)
                    if deleted_person_number == 0:
                        message = REMOVE_ELECTED_NOT_EXIST
                    else:
                        message = REMOVE_ELECTED_SUCCESS_MESSAGE

        return message

    def form_message_on_me_command(self, telegram_id: int) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "me".  Чтобы вывести всю информацию, мы должны знать о
        пользователе все: id, username, name, phone_number.
        :param telegram_id: Уникальный идентификатор пользователя в Telegram.
        :return: строку, которая соответствует ситуации пользователя:
            -- пользователь не известен;
            -- пользователь известен, но отсутствует номер телефона;
            -- пользователь полностью известен, вывод полной информации о нём.
        """
        person_info, phone_number_not_exists = self.get_full_information(telegram_id)
        if person_info is None:
            return f"{NOT_AVAILABLE_COMMAND}{UNKNOWN_USER}"
        if phone_number_not_exists:
            return f"{NOT_AVAILABLE_COMMAND}{INPUT_PHONE}"
        return f"{person_info[0]['firstname']}!\n{USER_NAME}{person_info[0]['telegram_username']}, \n{PHONE}{person_info[0]['phone_number']}."

    def form_message_on_start_command(self, telegram_user: User) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "start". Сообщение зависит от того,
        является ли пользователь новым для бота и имеется ли в базе данных информация о номере телефона пользователя.
        :param telegram_user: Сущность Telegram-пользователя;
        :return: Возвращает сообщение пользователю в ответ на команду start.
        """
        message = ""
        has_phone_number, created = self.create_person(
            telegram_id=telegram_user.id, telegram_username=telegram_user.username, firstname=telegram_user.first_name
        )
        if created:
            message = f"{HELLO}{telegram_user.first_name}{EXCLAMATION_MARK}{START_CREATED}"
        else:
            if has_phone_number:
                message = f"{HELLO}{telegram_user.first_name}{EXCLAMATION_MARK}{START_HAS_PHONE}"
            else:
                message = f"{HELLO}{telegram_user.first_name}{EXCLAMATION_MARK}{START_NO_PHONE}"
        return message

    def form_message_on_set_phone_command(self, firstname: str, telegram_id: int, phone_number: str) -> str:
        """
        Формирование сообщения для пользователя в ответ на команду "set_phone".
        :param firstname: имя пользователя;
        :param telegram_id: уникальный идентификатор чата с пользователем;
        :param phone_number: номер телефона пользователя.
        :return: Возвращает сообщение пользователю в ответ на команду start.
        """
        message = ""
        if (phone_number is None) or (phone_number == ""):
            message = INCORRECT_PHONE_FORMAT
        elif self.check_person_presence(telegram_id):  # если пользователь есть в бд бота
            phone_number = self.check_phone_number(phone_number)  # проверка телефона на корректность формата
            if phone_number is not None:
                self.set_phone_number(telegram_id, phone_number)
                message = f"{THANK}{firstname}{EXCLAMATION_MARK}{SAVE_PHONE}"
            else:
                message = INCORRECT_PHONE_FORMAT
        else:
            message = f"{firstname}{EXCLAMATION_MARK}{UNKNOWN_USER}"
        return message
