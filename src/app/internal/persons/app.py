from ninja import NinjaAPI

from app.internal.persons.data.repositories import PersonRepository
from app.internal.persons.domain.services import PersonService
from app.internal.persons.presentation.handlers import PersonHandlers
from app.internal.persons.presentation.routers import add_person_router


def get_person_service():
    person_repo = PersonRepository()
    person_service = PersonService(person_repo=person_repo)
    return person_service


def configure_person_api(api: NinjaAPI):
    person_service = get_person_service()
    person_handler = PersonHandlers(person_service=person_service)
    add_person_router(api, person_handler)


def get_person_handlers():
    person_service = get_person_service()
    return PersonHandlers(person_service)
