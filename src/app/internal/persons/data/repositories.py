from typing import List, Optional

from django.db import transaction

from app.internal.persons.data.models import Person
from app.internal.persons.domain.entities import PersonIn, PersonOut
from app.internal.persons.domain.repo_interface import IPersonRepository


class PersonRepository(IPersonRepository):
    def create_person(self, telegram_id: int, telegram_username: str, firstname: str) -> (bool, bool):
        """
        Создаёт новый объект пользователя с переданными параметрами, либо изменяет уже существующего.
        :param telegram_id: Уникальный идентификатор сообщений пользователя в Telegram.
        :param telegram_username: Имя пользователя в Telegram.
        :param firstname: Имя пользователя.
        :return: Возвращает кортеж, содержащий информацию о наличии телефона в объекте пользователя
        и создавался ли новый объект пользователя.
        """
        person, created = Person.objects.update_or_create(  # возвращает созданный объект и переменную логического типа:
            # был ли создан новый объект
            telegram_id=telegram_id,  # по этому аргументу производится попытка найти соответсвующий объект
            defaults={"telegram_username": telegram_username, "firstname": firstname}  # словарь defaults используется
            # для обновления объекта
        )
        phone_number_exists = person.phone_number != ""
        return phone_number_exists, created

    def check_person_presence(self, telegram_id: int) -> bool:
        """
        Проверяет присутствие пользователя в базе данных.
        :param telegram_id: Уникальный идентификатор пользователя в Telegram.
        :return: True, если пользователя с данным id существует. Иначе False.
        """
        return Person.objects.filter(telegram_id=telegram_id).exists()
        # Возвращает значение True, если QuerySet
        # содержит какие-либо результаты

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        """
        Находит объект пользователя в базе данных и устанавливает ему номер телефона.
        :param telegram_id: Уникальный идентификатор пользователя в Telegram.
        :param phone_number: Номер телефона пользователя.
        :return: Данный метод не имеет выходных значений.
        """
        Person.objects.filter(telegram_id=telegram_id).update(phone_number=phone_number)

    def get_person(self, telegram_id: int) -> Person:
        """
        Получение объекта пользователя по его уникальному идентификатору.
        :param telegram_id: Уникальный идентификатор пользователя в Telegram.
        :return: объект пользователя, который соответствует переданному уникальному идентификатору.
        """
        return Person.objects.filter(telegram_id=telegram_id).first()

    def get_full_information(self, telegram_id: int) -> (Optional[Person], bool):
        """
        Получение полной информации о пользователе: id, username, name, phone_number.
        :param telegram_id: Уникальный идентификатор пользователя в Telegram.
        :return: Кортеж из двух элементов: пользователь и логическая переменная, которая содержит информацию о том,
        имеет ли пользователь номер телефона.
        """
        person_info = Person.objects.filter(telegram_id=telegram_id).values(
            "firstname", "telegram_username", "phone_number"
        )
        if len(person_info) == 0:
            return None, False
        phone_number_not_exists = person_info[0]["phone_number"] == ""
        return person_info, phone_number_not_exists

    def get_person_by_telegram_username(self, telegram_username: str) -> Person | None:
        return Person.objects.filter(telegram_username=telegram_username).first()

    # def check_password(self, telegram_id: int, password_hash: bytes) -> bool:
    #     person = self.get_person(telegram_id)
    #     if person.password_hash is None:
    #         person.password_hash = password_hash
    #         person.save()
    #         return True
    #     return compare_passwords_hash(password_hash, bytes(person.password_hash))

    @transaction.atomic()
    def add_elected_person(self, holder_out: PersonOut, elected_person_out: PersonOut) -> bool:
        """
        Возвращает информацию о том, был ли добавлен пользователь в список избранных
        """
        holder = self.get_person(holder_out.telegram_id)
        elected_person = self.get_person(elected_person_out.telegram_id)
        exists_in_elected_list = self.has_this_elected_person(holder, elected_person)
        if not exists_in_elected_list:
            holder.elected_persons.add(elected_person)
            holder.save()
        return not exists_in_elected_list

    @transaction.atomic()
    def remove_elected_person(self, holder_telegram_id: int, elected_person_in: PersonIn) -> bool:
        """
        Возвращает количество удаленных из списка пользователей
        """
        holder = self.get_person(holder_telegram_id)
        elected_person = self.get_person(elected_person_in.telegram_id)
        exists_in_elected_list = self.has_this_elected_person(holder, elected_person)
        if not exists_in_elected_list:
            delete_count = 0
        else:
            holder.elected_persons.remove(elected_person)
            holder.save()
            delete_count = 1
        return delete_count != 0

    def show_elected_persons(self, holder_telegram_id: int) -> List[str]:
        """
        Отображение списка Telegram-имен избранных пользователей
        """
        elected_persons = Person.objects.filter(telegram_id=holder_telegram_id).values(
            "elected_persons__telegram_username"
        )
        if elected_persons[0]["elected_persons__telegram_username"] is None:
            return []
        return list(person["elected_persons__telegram_username"] for person in elected_persons)

    def has_this_elected_person(self, holder_in: PersonIn, elected_person_in: PersonIn) -> bool:
        """
        Проверяет наличие пользователя elected_person в списке избранных пользователя holder
        """
        exists = Person.objects.filter(
            telegram_id=holder_in.telegram_id, elected_persons__telegram_id=elected_person_in.telegram_id
        ).exists()
        return exists
