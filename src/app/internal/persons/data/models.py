from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Person(models.Model):
    """
    Данный класс представляет собой модель пользователя Telegram бота.
    Содержит поля и параметры хранения данных.
    Сопоставляется с одной таблицей базы данных.
    """

    telegram_id = models.PositiveIntegerField(
        primary_key=True,
        verbose_name="Уникальный идентификатор сообщений пользователя в Telegram",
    )
    telegram_username = models.CharField(
        max_length=255,
        unique=True,
        null=False,
        verbose_name="Имя пользователя в Telegram",
    )
    firstname = models.CharField(
        max_length=255,
        null=False,
        default="",
        verbose_name="Имя пользователя",
    )
    phone_number = models.CharField(default="", max_length=15, unique=True, verbose_name="Номер телефона пользователя")
    elected_persons = models.ManyToManyField(to="self", symmetrical=False)  # добавление отношения many to many
    # пользователя самого на себя. Получим таблицу с 2 внешними ключами Foreign Key, которые будут указывать на
    # записи таблицы Person. Отношение не симметричное т.к. если избранный пользователь находится в
    # списке избранных у текущего пользователя, то это не означает, что текущий пользователь находится в
    # списке избранных у избранного пользователя.
    password_hash = models.BinaryField(
        editable=True, null=True, max_length=65000, verbose_name="Хэш пароля пользователя"
    )

    objects = models.Manager()  # менеджер объектов

    def __str__(self) -> str:
        return f"person: {self.telegram_id}, {self.telegram_username}"

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели пользователя Person.
        """

        verbose_name = "Person"  # Удобочитаемое имя для поля, единственное число
        verbose_name_plural = "People"  # Удобочитаемое имя для поля, множественное число
