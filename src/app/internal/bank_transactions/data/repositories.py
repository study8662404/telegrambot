from typing import Optional

from _decimal import Decimal
from django.db import transaction
from django.db.models import F

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.bank_accounts.domain.entities import BankAccountIn
from app.internal.bank_transactions.data.models import BankTransaction
from app.internal.bank_transactions.domain.entities import BankTransactionOut
from app.internal.bank_transactions.domain.repo_interface import IBankTransactionRepository
from app.internal.persons.domain.entities import PersonIn
from app.internal.postcards.domain.entities import PostCardOut
from app.internal.postcards.domain.services import PostCardService


class BankTransactionRepository(IBankTransactionRepository):
    @transaction.atomic()
    def create_transaction(
        self, from_bank_account_in: BankAccountIn, to_bank_account_in: BankAccountIn, amount: Decimal
    ) -> BankTransaction:
        # Используем F для избежания ситуации гонки, чтобы выполнить операцию в SQL, а не в Python
        from_bank_account = BankAccount.objects.filter(number=from_bank_account_in.number).first()
        to_bank_account = BankAccount.objects.filter(number=to_bank_account_in.number).first()
        from_bank_account.amount = F("amount") - amount
        to_bank_account.amount = F("amount") + amount
        from_bank_account.save()
        to_bank_account.save()
        return BankTransaction.objects.create(sender=from_bank_account, recipient=to_bank_account, amount=amount)

    def get_transaction_info(
        self, from_bank_account_in: BankAccountIn
    ) -> (Optional[BankTransaction], Optional[BankTransaction]):
        sender_transaction_info = (
            BankTransaction.objects.filter(sender__number=from_bank_account_in.number)
            .values("recipient__number", "recipient__account_owner__telegram_username", "amount", "created_at")
            .order_by("recipient__number")
        )
        recipient_transaction_info = (
            BankTransaction.objects.filter(recipient__number=from_bank_account_in.number)
            .values("sender__number", "sender__account_owner__telegram_username", "amount", "created_at")
            .order_by("sender__number")
        )
        return sender_transaction_info, recipient_transaction_info

    def get_bank_interaction_persons(self, person_in: PersonIn):
        # person = get_person(person_telegram_id) ! Изменила сигнатуру метода!
        if person_in is None:
            return None, None
        recipients_username_query_set = (
            BankTransaction.objects.filter(sender__account_owner__telegram_id=person_in.telegram_id)
            .values("recipient__account_owner__telegram_username")
            .distinct()
        )
        senders_username_query_set = (
            BankTransaction.objects.filter(recipient__account_owner__telegram_id=person_in.telegram_id)
            .values("sender__account_owner__telegram_username")
            .distinct()
        )
        recipients_usernames = [
            username["recipient__account_owner__telegram_username"] for username in recipients_username_query_set
        ]
        sender_usernames = [
            username["sender__account_owner__telegram_username"] for username in senders_username_query_set
        ]
        usernames = set.union(set(recipients_usernames), set(sender_usernames))
        return usernames

    def show_new_transaction(self, postcard_service: PostCardService, message_arr, transactions):
        for bank_transaction in transactions:
            if not bank_transaction.was_source_viewed and bank_transaction.postcard is not None:
                postcard = postcard_service.get_post_card_by_transaction(bank_transaction)
                message_arr.append(
                    f"Отправитель: {bank_transaction.sender.number}, \n"
                    f"Получатель: {bank_transaction.recipient.number}, \n"
                    f"Сумма: {bank_transaction.amount}, \n"
                    f"Ссылка на изображение, связанное с переводом: {postcard.content.url}\n"
                )
                bank_transaction.was_source_viewed = True
                bank_transaction.save()

    def show_new_transactions(self, postcard_service: PostCardService, person_telegram_id):
        sender_transaction_info = BankTransaction.objects.filter(sender__account_owner__telegram_id=person_telegram_id)
        recipient_transaction_info = BankTransaction.objects.filter(
            recipient__account_owner__telegram_id=person_telegram_id
        )
        message = ["Информация о транзакциях с прикрепленными изображениями:\n"]
        self.show_new_transaction(postcard_service, message, sender_transaction_info)
        self.show_new_transaction(postcard_service, message, recipient_transaction_info)
        if len(message) == 1:
            return "У вас отсутствуют переводы с изображениями!"
        return "".join(message)

    def add_postcard_to_transaction(
        self, postcard_service: PostCardService, transaction_out: BankTransactionOut, postcard_out: PostCardOut
    ):
        post_card = postcard_service.get_postcard_by_id(postcard_out.id)
        bank_transaction = BankTransaction.objects.filter(id=transaction_out.id).first()
        bank_transaction.postcard = post_card
        bank_transaction.was_source_viewed = False
        bank_transaction.save(update_fields=("postcard", "was_source_viewed"))
