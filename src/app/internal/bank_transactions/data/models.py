from django.db import models

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.postcards.data.models import PostCard


class BankTransaction(models.Model):
    """
    Данный класс представляет собой модель банковской транзакции.
    Содержит поля и параметры хранения данных: отправитель денег, получатель, сумма перевода, время и дата транзакции.
    Сопоставляется с одной таблицей базы данных.
    """

    sender = models.ForeignKey(
        BankAccount,  # Т.к. информация о количестве денег хранится на счёте,
        # отправителем будет объект банковского счета
        related_name="money_sender",  # related_name позволяет обращаться из связанных объектов к тем,
        # от которых эта связь была создана
        verbose_name="Отправитель денег",  # verbose_name: Удобочитаемое имя для поля, используемого в поле метки
        on_delete=models.CASCADE,  # При удалении счёта происходит удаление получателя
    )
    recipient = models.ForeignKey(
        BankAccount, related_name="money_recipient", verbose_name="Получатель денег", on_delete=models.CASCADE
    )
    amount = models.DecimalField(
        max_digits=7, decimal_places=2, verbose_name="Сумма перевода"  # максимальная сумма перевода 99 999,99
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время банковской транзакции")
    postcard = models.ForeignKey(
        PostCard, on_delete=models.CASCADE, related_name="transaction_post_cards", blank=True, null=True
    )
    was_source_viewed = models.BooleanField(default=False)  # Просматриваются только те переводы,
    # которые пользователь еще не видел
    objects = models.Manager()  # менеджер объектов

    def __str__(self):
        return (
            f"Отправитель: {self.sender.account_owner.telegram_username} "
            f"Получатель: {self.recipient.account_owner.telegram_username} "
            f"Сумма: {self.amount}."
        )

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели транзакции BankTransaction
        """

        verbose_name = "Банковская транзакция"  # Удобочитаемое имя для поля, единственное число
        verbose_name_plural = "Банковские транзакции"  # Удобочитаемое имя для поля, множественное число
