from django.contrib import admin

from app.internal.bank_transactions.data.models import BankTransaction


@admin.register(BankTransaction)  # декоратор для регистрации классов ModelAdmin
class BankTransactionAdmin(admin.ModelAdmin):
    """
    Данный класс является представлением модели BankTransaction в интерфейсе администрирования.
    """

    fields = ["sender", "recipient", "amount"]
