import datetime

from telegram import Update
from telegram.ext import CallbackContext

from app.bot.services import send_message
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_transactions.domain.services import BankTransactionService
from app.internal.bank_transactions.presentation.messages import (
    ACCOUNT_STATEMENT_HELP,
    BANK_TRANSACTION_HELP_MESSAGE,
    CARD_STATEMENT_HELP,
    INCORRECT_ACCOUNT_FORMAT,
    INCORRECT_CARD_FORMAT,
)
from app.internal.persons.domain.services import PersonService
from app.internal.postcards.domain.services import PostCardService


class BankTransactionHandlers:
    def __init__(
        self,
        bank_transaction_service: BankTransactionService,
        bank_account_service: BankAccountService,
        person_service: PersonService,
        postcard_service: PostCardService,
        bank_card_service: BankCardService,
    ):
        self.bank_transaction_service = bank_transaction_service
        self.person_service = person_service
        self.postcard_service = postcard_service
        self.bank_account_service = bank_account_service
        self.bank_card_service = bank_card_service

    def do_transaction_with_postcard_handler(self, update: Update, context: CallbackContext) -> None:
        chat_id = update.effective_chat.id
        caption = update.message.caption
        caption_array = caption.split(" ")
        args_count = len(caption_array)
        if args_count == 0:
            message = (
                "Уважаемый пользователь! Укажите, пожалуйста, команду и соответствующие аргументы! Для просмотра "
                "доступных команд используйте команду /help."
            )
            send_message(context, chat_id, message)
            return
        command = caption_array[0][1:]
        try:
            file_id = update.message.photo[0].file_id
            initial_file_name = update.message.photo[0].file_unique_id
            file = context.bot.get_file(file_id)
            content = file.download_as_bytearray()
            file_name = f"{chat_id}_{datetime.datetime.now().timestamp()}_{initial_file_name}"
        except Exception as e:
            print(e)
            message = "При загрузке изображения возникала ошибка!"
            send_message(context, chat_id, message)
            return
        if command == "do_transaction":
            message = self.bank_transaction_service.do_transaction_with_post_card(
                self.person_service,
                self.bank_account_service,
                self.bank_card_service,
                self.postcard_service,
                update.effective_user.id,
                caption_array[1:],
                file_name,
                content,
            )
            send_message(context, chat_id, message)
            return
        else:
            message = "Отправка сообщения с изображением доступна только для команды /do_transaction."
            send_message(context, chat_id, message)
            return

    def do_transaction(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'do_transaction'.
        Совершение транзакции.
        Варианты использования команды /do_transaction:"
        1. /do_transaction номер_банковского_счета_отправителя номер_банковского_счёта_получателя сумма_перевода.
        2. /do_transaction номер_банковского_счета_отправителя номер_карты_получателя сумма_перевода.
        3. /do_transaction номер_банковского_счета_отправителя имя_получателя_в_Telegram сумма_перевода.
        4. /do_transaction номер_карты_отправителя номер_банковского_счёта_получателя сумма_перевода.
        5. /do_transaction номер_карты_отправителя номер_карты_получателя сумма_перевода.
        6. /do_transaction номер_карты_отправителя имя_получателя_в_Telegram сумма_перевода.
        7. /do_transaction имя_получателя_в_Telegram сумма_перевода.
        8. /do_transaction номер_банковского_счёта_получателя сумма_перевода.
        9. /do_transaction номер_карты_получателя сумма_перевода.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        args_len = len(context.args)
        message = ""
        if args_len == 2:
            message, transaction = self.bank_transaction_service.do_transaction_with_two_args(
                self.person_service,
                self.bank_account_service,
                self.bank_card_service,
                update.effective_user.id,
                context.args[0],
                context.args[1],
            )
        elif args_len == 3:
            message, transaction = self.bank_transaction_service.do_transaction_with_three_args(
                self.person_service,
                self.bank_account_service,
                self.bank_card_service,
                context.args[0],
                context.args[1],
                context.args[2],
            )
        else:
            message = BANK_TRANSACTION_HELP_MESSAGE
        send_message(context, update.effective_chat.id, message)

    def show_new_transactions(self, update: Update, context: CallbackContext) -> None:
        person_telegram_id = update.effective_user.id
        message = self.bank_transaction_service.show_new_transactions(self.postcard_service, person_telegram_id)
        send_message(context, update.effective_chat.id, message)

    def bank_interaction(self, update: Update, context: CallbackContext) -> None:
        message = self.bank_transaction_service.form_message_on_bank_interaction_command(
            self.person_service, update.effective_user.id
        )
        send_message(context, update.effective_chat.id, message)

    def account_statement(self, update: Update, context: CallbackContext) -> None:
        indicate_account_number = len(context.args) == 1
        message = ""
        if not indicate_account_number:
            message = ACCOUNT_STATEMENT_HELP
        else:
            account_number = context.args[0]
            valid = self.bank_account_service.check_bank_account_number_validity(account_number)
            if not valid:
                message = INCORRECT_ACCOUNT_FORMAT
            else:
                message = self.bank_transaction_service.form_message_on_account_statement_command(
                    self.bank_account_service, update.effective_user.id, account_number
                )
        send_message(context, update.effective_chat.id, message)

    def card_statement(self, update: Update, context: CallbackContext) -> None:
        indicate_card_number = len(context.args) == 1
        message = ""
        if not indicate_card_number:
            message = CARD_STATEMENT_HELP
        else:
            card_number = context.args[0]
            valid = self.bank_card_service.check_bank_card_number_validity(card_number)
            if not valid:
                message = INCORRECT_CARD_FORMAT
            else:
                message = self.bank_transaction_service.form_message_on_card_statement_command(
                    self.bank_account_service, card_number
                )
        send_message(context, update.effective_chat.id, message)
