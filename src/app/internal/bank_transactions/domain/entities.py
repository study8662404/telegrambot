from _decimal import Decimal
from ninja.orm import create_schema

from app.internal.bank_transactions.data.models import BankTransaction
from app.internal.persons.domain.entities import PersonOut

BankTransactionSchema = create_schema(BankTransaction)



class BankTransactionIn(BankTransactionSchema):
    # убрать наследование от BankTransactionSchema, прописать наследование от базовой Schema
    # добавить поля
    ...


class BankTransactionOut(BankTransactionSchema):
    ...
    # sender: PersonOut
    # recipient: PersonOut
    # amount: Decimal
