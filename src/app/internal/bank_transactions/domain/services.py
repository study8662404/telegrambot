from typing import List, Optional

from _decimal import Decimal

from app.internal.bank_accounts.domain.entities import BankAccountIn, BankAccountOut
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_transactions.data.models import BankTransaction
from app.internal.bank_transactions.domain.entities import BankTransactionOut
from app.internal.bank_transactions.domain.repo_interface import IBankTransactionRepository
from app.internal.bank_transactions.presentation.messages import (
    BANK_ACCOUNT_NOT_LINKED,
    BANK_CARD_NOT_LINKED,
    BANK_TRANSACTION_HELP_MESSAGE,
    BANK_TRANSACTION_INCORRECT_SUM_MESSAGE,
    BANK_TRANSACTION_NOT_ELECTED_INCORRECT_MESSAGE,
    BANK_TRANSACTION_NOT_RECEIVER_ACCOUNT_MESSAGE,
    BANK_TRANSACTION_NOT_REGISTER_RECEIVER_MESSAGE,
    BANK_TRANSACTION_NOT_REGISTER_SENDER_MESSAGE,
    BANK_TRANSACTION_RECEIVER_INCORRECT_MESSAGE,
    BANK_TRANSACTION_SENDER_INCORRECT_MESSAGE,
    BANK_TRANSACTION_SEVERAL_RECEIVER_ACCOUNTS_MESSAGE,
    BANK_TRANSACTION_SEVERAL_SENDER_ACCOUNTS_MESSAGE,
    BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE,
    BANK_TRANSACTION_SUCCESS_MESSAGE,
    BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE,
    BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE,
    INTERACTION_LIST_PERSON,
    LINE_BREAK,
    NO_BANK_ACCOUNT,
    NO_TRANSACTION_INFORMATION,
)
from app.internal.persons.domain.entities import PersonIn
from app.internal.persons.domain.services import PersonService
from app.internal.postcards.domain.entities import PostCardOut
from app.internal.postcards.domain.services import PostCardService


class BankTransactionService:
    def __init__(self, bank_transaction_repo: IBankTransactionRepository):
        self._bank_transaction_repo = bank_transaction_repo

    def create_transaction(
        self, from_bank_account: BankAccountIn, to_bank_account: BankAccountIn, amount: Decimal
    ) -> BankTransactionOut:
        transaction = self._bank_transaction_repo.create_transaction(from_bank_account, to_bank_account, amount)
        return BankTransactionOut.from_orm(transaction)

    def get_transaction_info(
        self, from_bank_account: BankAccountIn
    ) -> (Optional[BankTransaction], Optional[BankTransaction]):
        sender_transaction_info, recipient_transaction_info = self._bank_transaction_repo.get_transaction_info(
            from_bank_account
        )
        return sender_transaction_info, recipient_transaction_info

    def get_bank_interaction_persons(self, person: PersonIn):
        usernames = self._bank_transaction_repo.get_bank_interaction_persons(person)
        return usernames

    def do_transaction_with_post_card(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        bank_card_service: BankCardService,
        postcard_service: PostCardService,
        person_telegram_id,
        caption_array: List[str],
        file_name: str,
        content: bytes,
    ):
        args_len = len(caption_array)
        if args_len != 2 and args_len != 3:
            return BANK_TRANSACTION_HELP_MESSAGE
        transaction = None
        message = ""
        if args_len == 2:
            message, transaction = self.do_transaction_with_two_args(
                person_service,
                bank_account_service,
                bank_card_service,
                person_telegram_id,
                caption_array[0],
                caption_array[1],
            )
        elif args_len == 3:
            message, transaction = self.do_transaction_with_three_args(
                person_service,
                bank_account_service,
                bank_card_service,
                caption_array[0],
                caption_array[1],
                caption_array[2],
            )
        postcard_out = postcard_service.create_post_card(file_name, content)
        self._bank_transaction_repo.add_postcard_to_transaction(postcard_service, transaction, postcard_out)
        return message

    def do_transaction_with_two_args(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        bank_card_service: BankCardService,
        from_user_telegram_id: int,
        to_money: str,
        amount: str,
    ) -> (str, BankTransactionOut):
        """
        Совершение транзакции, если команда для бота содержит 2 аргумента.
        :param from_user_telegram_id: Уникальный идентификатор пользователя, приславшего сообщение
        :param to_money: Получатель денег: карта, аккаунт, имя в Telegram
        :param amount: Сумма перевода
        :return: Сообщение пользователю о результате совершения транзакции
        """
        if not amount.isdecimal():
            return BANK_TRANSACTION_INCORRECT_SUM_MESSAGE, None
        amount = Decimal(amount)
        to_is_bank_card = bank_card_service.check_bank_card_number_validity(to_money)
        to_is_bank_account = bank_account_service.check_bank_account_number_validity(to_money)
        to_is_user = (
            person_service.validate_telegram_username(to_money) and not to_is_bank_account and not to_is_bank_card
        )
        return self.form_message_on_do_transfer_command_with_two_args(
            person_service,
            bank_account_service,
            from_user_telegram_id,
            to_is_bank_card,
            to_is_bank_account,
            to_is_user,
            to_money,
            amount,
        )

    def do_transaction_with_three_args(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        bank_card_service: BankCardService,
        from_money: str,
        to_money: str,
        amount: str,
    ) -> (str, BankTransactionOut):
        """
        Совершение транзакции, если команда для бота содержит 3 аргумента.
        :param from_money: Отправитель денег: карта, аккаунт,
        :param to_money: Получатель денег: карта, аккаунт, имя в Telegram
        :param amount: Сумма перевода
        :return: Сообщение пользователю о результате совершения транзакции
        """
        from_is_bank_card = bank_card_service.check_bank_card_number_validity(from_money)
        to_is_bank_card = bank_card_service.check_bank_card_number_validity(to_money)
        from_is_bank_account = bank_account_service.check_bank_account_number_validity(from_money)
        to_is_bank_account = bank_account_service.check_bank_account_number_validity(to_money)
        to_is_user = (
            person_service.validate_telegram_username(to_money) and not to_is_bank_account and not to_is_bank_card
        )

        if not amount.isdecimal():
            return BANK_TRANSACTION_INCORRECT_SUM_MESSAGE
        amount = Decimal(amount)
        return self.form_message_on_do_transfer_with_three_args(
            person_service,
            bank_account_service,
            from_is_bank_card,
            to_is_bank_card,
            from_is_bank_account,
            to_is_bank_account,
            to_is_user,
            amount,
            from_money,
            to_money,
        )

    def form_message_on_do_transfer_command_with_two_args(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_user_telegram_id: int,
        to_is_bank_card: bool,
        to_is_bank_account: bool,
        to_is_user: bool,
        to_money: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Формирование сообщения для пользователя в ответ на команду "do_transaction" с 2 аргументами.
        :param from_user_telegram_id: Уникальный идентификатор пользователя, приславшего сообщение.
        :param to_is_bank_card: Логическая переменная, которая сигнализирует о том, совершается ли транзакция на карту.
        :param to_is_bank_account: Логическая переменная, которая сигнализирует о том, совершается ли транзакция на
         банковский аккаунт.
        :param to_is_user: Логическая переменная, которая сигнализирует о том, совершается ли транзакция по имени
        пользователя в Telegram.
        :param to_money: Карта/аккаунт/имя пользователя в Telegram получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction.
        """
        from_person = person_service.get_person(from_user_telegram_id)
        if from_person is None:
            return BANK_TRANSACTION_NOT_REGISTER_SENDER_MESSAGE, None
        from_bank_accounts = bank_account_service.get_bank_account_by_owner(from_person)
        accounts_count = len(from_bank_accounts)
        if accounts_count == 0:
            return NO_BANK_ACCOUNT, None
        if accounts_count != 1:
            return BANK_TRANSACTION_SEVERAL_SENDER_ACCOUNTS_MESSAGE, None
        if to_is_user:
            return self.transfer_money_to_person(
                person_service, bank_account_service, from_bank_accounts[0], to_money, amount
            )
        elif to_is_bank_card:
            return self.account_card_transfer_money(
                person_service, bank_account_service, from_bank_accounts[0].number, to_money, amount
            )
        elif to_is_bank_account:
            return self.account_account_transfer_money(
                person_service, bank_account_service, from_bank_accounts[0].number, to_money, amount
            )
        else:
            return BANK_TRANSACTION_HELP_MESSAGE, None

    def form_message_on_do_transfer_with_three_args(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_is_bank_card: bool,
        to_is_bank_card: bool,
        from_is_bank_account: bool,
        to_is_bank_account: bool,
        to_is_user: bool,
        amount: Decimal,
        from_money: str,
        to_money: str,
    ) -> (str, BankTransactionOut):
        """
        Формирование сообщения для пользователя в ответ на команду "do_transaction" с 3 аргументами.
        :param from_is_bank_card: Логическая переменная, которая сигнализирует о том,
        что совершается транзакция с карты пользователя.
        :param from_is_bank_account: Логическая переменная, которая сигнализирует о том,
        что совершается транзакция с банковского аккаунта пользователя.
        :param to_is_bank_card: Логическая переменная, которая сигнализирует о том, совершается ли транзакция на карту.
        :param to_is_bank_account: Логическая переменная, которая сигнализирует о том, совершается ли транзакция на
        банковский аккаунт.
        :param to_is_user: Логическая переменная, которая сигнализирует о том, совершается ли транзакция по имени
        пользователя в Telegram.
        :param from_money: Карта/аккаунт отправителя.
        :param amount: Сумма перевода.
        :param to_money: Карта/аккаунт/имя пользователя в Telegram получателя.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction.
        """
        if from_is_bank_card and to_is_bank_card:
            return self.card_card_transfer_money(person_service, bank_account_service, from_money, to_money, amount)
        elif from_is_bank_card and to_is_bank_account:
            return self.card_account_transfer_money(person_service, bank_account_service, from_money, to_money, amount)
        elif from_is_bank_account and to_is_bank_card:
            return self.account_card_transfer_money(person_service, bank_account_service, from_money, to_money, amount)
        elif from_is_bank_account and to_is_bank_account:
            return self.account_account_transfer_money(
                person_service, bank_account_service, from_money, to_money, amount
            )
        elif from_is_bank_account and to_is_user:
            from_account = bank_account_service.get_bank_account_by_number(from_money)
            return self.transfer_money_to_person(person_service, bank_account_service, from_account, to_money, amount)
        elif from_is_bank_card and to_is_user:
            from_account = bank_account_service.get_bank_account_by_card_number(from_money)
            return self.transfer_money_to_person(person_service, bank_account_service, from_account, to_money, amount)
        else:
            return BANK_TRANSACTION_HELP_MESSAGE, None

    def transfer_money_to_person(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_bank_account: BankAccountOut,
        to_telegram_username: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Перевод денежных средств по имени пользователя в Telegram.
        :param from_bank_account: Банковский аккаунт отправителя.
        :param to_telegram_username: Имя пользователя в Telegram получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction с аргументом ---
         именем пользователя-получателя.
        """
        to_person = person_service.get_person_by_telegram_username(to_telegram_username)
        if to_person is None:
            return BANK_TRANSACTION_NOT_REGISTER_RECEIVER_MESSAGE, None
        to_bank_accounts = bank_account_service.get_bank_account_by_owner(to_person)
        to_bank_account_count = len(to_bank_accounts)
        if to_bank_account_count == 0:
            return BANK_TRANSACTION_NOT_RECEIVER_ACCOUNT_MESSAGE, None
        if to_bank_account_count != 1:
            return BANK_TRANSACTION_SEVERAL_RECEIVER_ACCOUNTS_MESSAGE, None
        return self.transfer_money(person_service, from_bank_account, to_bank_accounts[0], amount)

    def card_card_transfer_money(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_card_number: str,
        to_card_number: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Перевод денежных средств с карты на карту.
        :param from_card_number: Номер банковской карты отправителя.
        :param to_card_number: Номер банковской карты получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction
        """
        from_account = bank_account_service.get_bank_account_by_card_number(from_card_number)
        to_account = bank_account_service.get_bank_account_by_card_number(to_card_number)
        return self.transfer_money(person_service, from_account, to_account, amount)

    def account_card_transfer_money(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_account_number: str,
        to_card_number: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Перевод денежных средств с аккаунта на карту.
        :param from_account_number: Номер банковского аккаунта отправителя.
        :param to_card_number: Номер банковской карты получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction
        """
        from_account = bank_account_service.get_bank_account_by_number(from_account_number)
        to_account = bank_account_service.get_bank_account_by_card_number(to_card_number)
        return self.transfer_money(person_service, from_account, to_account, amount)

    def card_account_transfer_money(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_card_number: str,
        to_account_number: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Перевод денежных средств с карты на аккаунт.
        :param from_card_number: Номер банковской карты отправителя.
        :param to_account_number: Номер банковского аккаунта получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction
        """
        from_account = bank_account_service.get_bank_account_by_card_number(from_card_number)
        to_account = bank_account_service.get_bank_account_by_number(to_account_number)
        return self.transfer_money(person_service, from_account, to_account, amount)

    def account_account_transfer_money(
        self,
        person_service: PersonService,
        bank_account_service: BankAccountService,
        from_account_number: str,
        to_account_number: str,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Перевод денежных средств с аккаунта на аккаунт.
        :param from_account_number: Номер банковского аккаунта отправителя.
        :param to_account_number: Номер банковского аккаунта получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction
        """
        from_account = bank_account_service.get_bank_account_by_number(from_account_number)
        to_account = bank_account_service.get_bank_account_by_number(to_account_number)
        return self.transfer_money(person_service, from_account, to_account, amount)

    def check_available_transfer(
        self,
        person_service: PersonService,
        from_bank_account: BankAccountOut,
        to_bank_account: BankAccountOut,
        amount: Decimal,
    ) -> bool:
        """
        Проверка возможности совершения транзакции.
        :param from_bank_account: Сущность банковского аккаунта отправителя.
        :param to_bank_account: Сущность банковского аккаунта получателя.
        :param amount: Сумма перевода.
        :return: Возвращает логическую переменную, является ли транзакция валидной
        """
        from_person = from_bank_account.account_owner
        to_person = to_bank_account.account_owner
        return person_service.has_this_elected_person(from_person, to_person) and (from_bank_account.amount > amount)

    def transfer_money(
        self,
        person_service: PersonService,
        from_bank_account: BankAccountOut,
        to_bank_account: BankAccountOut,
        amount: Decimal,
    ) -> (str, BankTransactionOut):
        """
        Совершение транзакции.
        :param from_bank_account: Сущность банковского аккаунта отправителя.
        :param to_bank_account: Сущность банковского аккаунта получателя.
        :param amount: Сумма перевода.
        :return: Возвращает сообщение пользователю в ответ на команду do_transaction
        """
        transaction = None
        if from_bank_account is None:
            return BANK_TRANSACTION_SENDER_INCORRECT_MESSAGE, transaction
        elif to_bank_account is None:
            return BANK_TRANSACTION_RECEIVER_INCORRECT_MESSAGE, transaction

        available_transfer = self.check_available_transfer(person_service, from_bank_account, to_bank_account, amount)
        if not available_transfer:
            return BANK_TRANSACTION_NOT_ELECTED_INCORRECT_MESSAGE, transaction
        transaction = self.create_transaction(from_bank_account, to_bank_account, amount)
        return (
            f"{BANK_TRANSACTION_SUCCESS_MESSAGE}"
            f"{BANK_TRANSACTION_SUCCESS_SENDER_ACCOUNT_NUMBER_MESSAGE}{from_bank_account.number}.\n"
            f"{BANK_TRANSACTION_SUCCESS_RECEIVER_ACCOUNT_NUMBER_MESSAGE}{to_bank_account.number}.\n"
            f"{BANK_TRANSACTION_SUCCESS_AMOUNT_MESSAGE}{transaction.amount}."
            # f"\n{BANK_TRANSACTION_SUCCESS_DATE_TIME_MESSAGE}{transaction.created_at}."
        ), transaction

    def show_new_transactions(self, postcard_service: PostCardService, person_telegram_id):
        return self._bank_transaction_repo.show_new_transactions(postcard_service, person_telegram_id)

    def add_postcard_to_transaction(
        self, postcard_service: PostCardService, transaction_out: BankTransactionOut, postcard_out: PostCardOut
    ):
        return self._bank_transaction_repo.add_postcard_to_transaction(postcard_service, transaction_out, postcard_out)

    def form_message_on_bank_interaction_command(self, person_service: PersonService, person_telegram_id: int) -> str:
        person = person_service.get_person(person_telegram_id)
        usernames = self.get_bank_interaction_persons(person)
        message = [INTERACTION_LIST_PERSON]
        for username in usernames:
            message.append(username)
        return LINE_BREAK.join(message)

    def form_message_from_transaction_info(self, sender_transaction_info, recipient_transaction_info):
        message = ["Информация о транзакциях:\n"]
        for transaction in sender_transaction_info:
            message.append(
                f"Получатель: {transaction['recipient__account_owner__telegram_username']},"
                f" номер счёта получателя: {transaction['recipient__number']},"
                f" сумма: {transaction['amount']},"
                f" время совершения транзакции: {transaction['created_at']}.\n"
            )
        for transaction in recipient_transaction_info:
            message.append(
                f"Отправитель: {transaction['sender__account_owner__telegram_username']},"
                f" номер счёта отправителя: {transaction['sender__number']},"
                f" сумма: {transaction['amount']},"
                f" время совершения транзакции: {transaction['created_at']}.\n"
            )
        if len(message) == 1:
            return NO_TRANSACTION_INFORMATION
        return LINE_BREAK.join(message)

    def form_message_on_account_statement_command(
        self, bank_account_service: BankAccountService, user_telegram_id: int, account_number: str
    ) -> str:
        bank_account = bank_account_service.get_bank_account_by_number(account_number)
        if bank_account is None:
            return BANK_ACCOUNT_NOT_LINKED
        sender_transaction_info, recipient_transaction_info = self.get_transaction_info(bank_account)
        return self.form_message_from_transaction_info(sender_transaction_info, recipient_transaction_info)

    def form_message_on_card_statement_command(self, bank_account_service: BankAccountService, card_number: str) -> str:
        bank_account = bank_account_service.get_bank_account_by_card_number(card_number)
        if bank_account is None:
            return BANK_CARD_NOT_LINKED
        sender_transaction_info, recipient_transaction_info = self.get_transaction_info(bank_account)
        return self.form_message_from_transaction_info(sender_transaction_info, recipient_transaction_info)
