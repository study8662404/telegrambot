from typing import Optional

from _decimal import Decimal

from app.internal.bank_accounts.domain.entities import BankAccountIn
from app.internal.bank_transactions.data.models import BankTransaction
from app.internal.bank_transactions.domain.entities import BankTransactionOut
from app.internal.persons.domain.entities import PersonIn
from app.internal.postcards.domain.entities import PostCardOut
from app.internal.postcards.domain.repo_interface import IPostCardRepository
from app.internal.postcards.domain.services import PostCardService


class IBankTransactionRepository:
    def create_transaction(
        self, from_bank_account: BankAccountIn, to_bank_account: BankAccountIn, amount: Decimal
    ) -> BankTransaction:
        ...

    def get_transaction_info(
        self, from_bank_account: BankAccountIn
    ) -> (Optional[BankTransaction], Optional[BankTransaction]):
        ...

    def get_bank_interaction_persons(self, person: PersonIn):
        ...

    def show_new_transactions(self, postcard_service: PostCardService, person_telegram_id):
        ...

    def add_postcard_to_transaction(
        self, postcard_service: PostCardService, transaction_out: BankTransactionOut, postcard_out: PostCardOut
    ):
        ...
