from app.internal.bank_accounts.app import get_bank_account_service
from app.internal.bank_cards.app import get_bank_card_service
from app.internal.bank_transactions.data.repositories import BankTransactionRepository
from app.internal.bank_transactions.domain.services import BankTransactionService
from app.internal.bank_transactions.presentation.handlers import BankTransactionHandlers
from app.internal.persons.app import get_person_service
from app.internal.postcards.app import get_postcard_service


def get_bank_transaction_service():
    bank_transaction_repo = BankTransactionRepository()
    bank_transaction_service = BankTransactionService(bank_transaction_repo=bank_transaction_repo)
    return bank_transaction_service


def get_bank_transaction_handlers():
    bank_transaction_service = get_bank_transaction_service()
    bank_account_service = get_bank_account_service()
    bank_card_service = get_bank_card_service()
    postcard_service = get_postcard_service()
    person_service = get_person_service()
    return BankTransactionHandlers(
        bank_transaction_service, bank_account_service, person_service, postcard_service, bank_card_service
    )
