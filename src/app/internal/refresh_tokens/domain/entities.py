from ninja.orm import create_schema

from app.internal.refresh_tokens.data.models import RefreshToken

RefreshTokenSchema = create_schema(RefreshToken)


class RefreshTokenIn(RefreshTokenSchema):
    ...


class RefreshTokenOut(RefreshTokenSchema):
    ...
