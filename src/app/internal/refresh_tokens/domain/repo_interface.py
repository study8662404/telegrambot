from app.internal.persons.data.models import Person
from app.internal.persons.domain.entities import PersonIn
from app.internal.refresh_tokens.data.models import RefreshToken


class IRefreshTokenRepository:
    def create_refresh_token(self, jti, person: PersonIn):
        ...

    def get_refresh_token_by_jti(self, jti: str) -> RefreshToken | None:
        ...

    def revoke_refresh_token_by_jti(self, jti):
        ...
