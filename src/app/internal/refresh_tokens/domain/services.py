from datetime import datetime, timedelta
from typing import Any

import jwt

from app.internal.persons.domain.entities import PersonIn
from app.internal.refresh_tokens.data.repositories import IRefreshTokenRepository
from app.internal.refresh_tokens.domain.entities import RefreshTokenOut
from config.settings import JWT_ENCODE_ALGORITHM, JWT_SECRET

ACCESS_TOKEN_TTL = timedelta(days=7)  # время жизни access-токена
REFRESH_TOKEN_TTL = timedelta(days=30)  # время жизни refresh-токена
EXPIRES_AT_FIELD = "exp"  # Время в формате Unix Time, определяющее момент, когда токен станет невалидным (expiration).
TELEGRAM_ID_FIELD = "sub"  # чувствительная к регистру строка или URI,
# которая является уникальным идентификатором стороны, о которой содержится информация в данном токене (subject).
# Значения с этим ключом должны быть уникальны в контексте стороны, генерирующей JWT.
JTI_FIELD = "jti"


class RefreshTokenService:
    def __init__(self, refresh_token_repo: IRefreshTokenRepository):
        self._refresh_token_repo = refresh_token_repo

    def create_refresh_token(self, jti, person: PersonIn):
        return self._refresh_token_repo.create_refresh_token(jti, person)

    def get_refresh_token_by_jti(self, jti: str) -> RefreshTokenOut | None:
        return self._refresh_token_repo.get_refresh_token_by_jti(jti)

    def revoke_refresh_token_by_jti(self, jti: str):
        return self._refresh_token_repo.revoke_refresh_token_by_jti(jti)

    def generate_access_token(self, person: PersonIn):
        """
        Генерация access-токена
        """
        expires_at = (datetime.utcnow() + ACCESS_TOKEN_TTL).timestamp()  # информация о дате и времени,
        # когда нужно будет получать новый access-токен
        access_token = jwt.encode(
            payload={TELEGRAM_ID_FIELD: person.telegram_id, EXPIRES_AT_FIELD: expires_at},
            key=JWT_SECRET,
            algorithm=JWT_ENCODE_ALGORITHM,
        )
        return access_token, expires_at

    def generate_refresh_token(self, person: PersonIn):
        """
        Генерация refresh-токена
        """
        expires_at = (datetime.utcnow() + REFRESH_TOKEN_TTL).timestamp()  # информация о дате и времени,
        # когда нужно будет получать новый refresh-токен
        refresh_token = jwt.encode(
            payload={TELEGRAM_ID_FIELD: person.telegram_id, EXPIRES_AT_FIELD: expires_at},
            key=JWT_SECRET,
            algorithm=JWT_ENCODE_ALGORITHM,
        )
        return self._refresh_token_repo.create_refresh_token(refresh_token, person)

    def decode_token(self, encoded_token: str) -> dict[str, Any]:
        """
        Декодирование токена и получение полезной нагрузки, им зашифрованной
        """
        return jwt.decode(jwt=encoded_token, key=JWT_SECRET, algorithms=[JWT_ENCODE_ALGORITHM])

    def validate_token(self, payload, person: PersonIn):
        """
        Валидация токена: проверка того, что совпадает человек, который отправил сообщение, с тем,
        для которого этот токен генерировался
        """
        return payload[TELEGRAM_ID_FIELD] == person.telegram_id
