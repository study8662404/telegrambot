from datetime import datetime

import jwt
from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from ninja.security import APIKeyHeader, HttpBearer

from app.internal.refresh_tokens.app import get_refresh_token_service
from app.internal.refresh_tokens.domain.services import EXPIRES_AT_FIELD, TELEGRAM_ID_FIELD

AUTHORIZATION_HEADER = "Authorization"
BEARER = "Bearer "
ALLOWED_PATH = "/api/person/me/"


class HTTJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        bearer_id = None
        r_path = request.path
        if ALLOWED_PATH not in r_path:
            return None
        if AUTHORIZATION_HEADER in request.headers:
            auth_header = request.headers[AUTHORIZATION_HEADER]
            if len(auth_header) < len(BEARER):
                return None
            token = auth_header[len(BEARER) :]
            bearer_id, check_message = self.check_access_token(token)
            if not bearer_id:
                return None
        else:
            return None
        # request.bearer_id = bearer_id
        # return self.get_response(request)
        return token

    def check_access_token(self, token):
        payload = None
        try:
            payload = get_refresh_token_service().decode_token(token)
        except:
            return None, "Invalid token"
        telegram_id = payload[TELEGRAM_ID_FIELD]
        expires_at = payload[EXPIRES_AT_FIELD]
        time_now = datetime.utcnow().timestamp()
        time_delta = expires_at - time_now
        if time_delta < 0:
            return None, "Token has been expired"
        return telegram_id, "OK"


# как в лекции:
# try:
#     payload = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
#     exp_time = datetime.fromtimestamp(payload["exp"])
#     datetime_now = datetime.now()
#     if exp_time < datetime_now:
#         return None
#     user = get_user_model().objects.get(id=payload["id"])
#     request.user = user
# except:
#     return None
#
# return token


# как было до этого у меня
# from datetime import datetime
# from http import HTTPStatus
# import jwt
# from django.contrib.auth.backends import BaseBackend
# from django.http import HttpRequest, HttpResponse
# from ninja.security import HttpBearer
# from app.internal.services.date_time_service import get_time_now
# from app.refresh_tokens.app import get_service
# from app.refresh_tokens.domain.services import TELEGRAM_ID_FIELD, EXPIRES_AT_FIELD
#
# AUTHORIZATION_HEADER = "Authorization"
# BEARER = "Bearer "
# NO_AUTH_ALLOWED_PATHS = {"/admin/", "/api/update_tokens/", "/api/login/", "/api/logout/"}
#
#
# class JWTAuthenticationMiddleware(BaseBackend):
#     def __init__(self, get_response):
#         self.get_response = get_response
#
#     def __call__(self, request):
#         bearer_id = None
#         r_path = request.path
#         for path in NO_AUTH_ALLOWED_PATHS:
#             if path in r_path:
#                 return self.get_response(request)
#         if AUTHORIZATION_HEADER in request.headers:
#             auth_header = request.headers[AUTHORIZATION_HEADER]
#             if len(auth_header) < len(BEARER):
#                 return HttpResponse("Unauthorized", status=HTTPStatus.UNAUTHORIZED)
#             token = auth_header[len(BEARER) :]
#             bearer_id, check_message = self.check_access_token(token)
#             if not bearer_id:
#                 return HttpResponse(check_message, status=HTTPStatus.UNAUTHORIZED)
#         else:
#             return HttpResponse("Unauthorized", status=HTTPStatus.UNAUTHORIZED)
#         request.bearer_id = bearer_id
#         return self.get_response(request)
#
#     def check_access_token(self, token):
#         refresh_token_service = get_refresh_token_service()
#         payload = None
#         try:
#             payload = refresh_token_service.decode_token(token)
#         except:
#             return None, "Invalid token"
#         telegram_id = payload[TELEGRAM_ID_FIELD]
#         expires_at = payload[EXPIRES_AT_FIELD]
#         time_now = get_time_now()
#         time_delta = expires_at - time_now
#         if time_delta < 0:
#             return None, "Token has been expired"
#         return telegram_id, "OK"
