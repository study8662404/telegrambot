import uuid

from django.db import models

from app.internal.persons.data.models import Person


class RefreshToken(models.Model):
    """
    Данный класс представляет собой модель выданного токена для пользователя Telegram бота.
    Именно модель Refresh-токена т.к. он живет долго и нужен для обновления Access-токенов
    """

    jti = models.CharField(max_length=255, primary_key=True, verbose_name="Уникальный идентификатор выданного токена")
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="refresh_tokens",
        verbose_name="Пользователь, для которого выдается токен",
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время создания токена")
    revoked = models.BooleanField(default=False, verbose_name="Информация о том, был ли токен отозван")
    device_id = models.UUIDField(  # Поле для хранения универсальных уникальных идентификаторов.
        default=uuid.uuid4,  # База данных не генерирует это значение автоматически, поэтому используется значение по
        # умолчанию
        unique=True,
        verbose_name="Уникальный идентификатор устройства"  # Нужно для того, чтобы отзывать не все токены, а только
        # те, которые относятся к конкретному устройству
    )
    objects = models.Manager()  # менеджер объектов

    def __str__(self) -> str:
        return f"token {self.jti}, {self.created_at}"

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели пользователя IssuedToken.
        """

        verbose_name = "Токен"  # Удобочитаемое имя для поля, единственное число
        verbose_name_plural = "Токены"  # Удобочитаемое имя для поля, множественное число
