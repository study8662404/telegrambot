from app.internal.persons.data.models import Person
from app.internal.persons.domain.entities import PersonIn
from app.internal.refresh_tokens.data.models import RefreshToken
from app.internal.refresh_tokens.domain.entities import RefreshTokenOut
from app.internal.refresh_tokens.domain.repo_interface import IRefreshTokenRepository


class RefreshTokenRepository(IRefreshTokenRepository):
    def create_refresh_token(self, jti: str, person_in: PersonIn):
        """
        Создание refresh-токена
        """
        person = Person.objects.filter(telegram_id=person_in.telegram_id).first()
        if person is None:
            return None
        token = RefreshToken.objects.create(jti=jti, person=person)
        return RefreshTokenOut.from_orm(token)

    def get_refresh_token_by_jti(self, jti: str) -> RefreshToken | None:
        """
        Получение refresh-токена по его уникальному идентификатору.
        При этом для избежания проблемы N + 1 запроса, используется select_related.
        """
        return RefreshToken.objects.select_related("person").filter(jti=jti).first()

    def revoke_refresh_token_by_jti(self, jti: str):
        """
        Отзыв refresh-токена по его уникальному идентификатору
        """
        refresh_token = RefreshToken.objects.filter(jti=jti).first()
        if refresh_token is None:
            return False
        refresh_token.revoked = "True"
        refresh_token.save()
        return True
