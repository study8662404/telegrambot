from ninja import NinjaAPI, Router

from app.internal.broad.responses import ErrorResponse, JWTResponse, SchemaResponse, SuccessResponse
from app.internal.refresh_tokens.presentation.handlers import RefreshTokenHandlers


def get_refresh_token_router(refresh_token_handlers: RefreshTokenHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login",
        ["POST"],
        refresh_token_handlers.login,
        response={200: JWTResponse, 401: ErrorResponse, 403: ErrorResponse},
        auth=None,
    )

    router.add_api_operation(
        "/logout",
        ["POST"],
        refresh_token_handlers.logout,
        response={200: SuccessResponse, 401: ErrorResponse},
        auth=None,
    )

    router.add_api_operation(
        "/update_tokens",
        ["POST"],
        refresh_token_handlers.update_tokens,
        response={200: JWTResponse, 401: ErrorResponse},
        auth=None,
    )

    return router


def add_refresh_token_router(api: NinjaAPI, refresh_token_handlers: RefreshTokenHandlers):
    refresh_token_handler = get_refresh_token_router(refresh_token_handlers)
    api.add_router("auth", refresh_token_handler)
    return api
