from django.contrib import admin

from app.internal.refresh_tokens.data.models import RefreshToken


@admin.register(RefreshToken)
class RefreshTokenAdmin(admin.ModelAdmin):
    fields = ["jti", "device_id", "person", "revoked"]
