from datetime import datetime
from http import HTTPStatus

from django.http import HttpRequest

from app.internal.broad.responses import ErrorResponse, JWTResponse, SchemaResponse, SuccessResponse
from app.internal.persons.app import get_person_service
from app.internal.refresh_tokens.domain.services import EXPIRES_AT_FIELD, RefreshTokenService

LOGIN_FIELD = "login"
PASSWORD_FIELD = "password"
REFRESH_TOKEN_FIELD = "refresh_token"


class RefreshTokenHandlers:
    def __init__(self, refresh_token_service: RefreshTokenService):
        self.refresh_token_service = refresh_token_service
        self.person_service = get_person_service()

    def logout(self, request: HttpRequest):
        """
        Выход пользователя с приложения со всех устройств. Удаление refresh-токена пользователя.
        Дальнейший вход будет по логину и паролю.
        """
        person_login = request.headers[LOGIN_FIELD]
        refresh_token_jti = request.headers[REFRESH_TOKEN_FIELD]
        person = self.person_service.get_person_by_telegram_username(telegram_username=person_login)
        if not person:
            return 401, ErrorResponse(message="401 Unauthorized")
        revoked = self.refresh_token_service.revoke_refresh_token_by_jti(jti=refresh_token_jti)
        if not revoked:
            return 401, ErrorResponse(message="401 Unauthorized")
        return 200, SuccessResponse(success=revoked)

    def login(self, request: HttpRequest):
        """
        Логика работы: пользователь вводит логин и пароль, клиент отправляет их на сервер,
        сервер генерирует токены (access и refresh), клиент их сохраняет для пользователя.
        После этого, юзер может не вводить пароль и логин каждый раз при входе в приложение.
        """
        login = request.POST[LOGIN_FIELD]
        person = self.person_service.get_person_by_telegram_username(telegram_username=login)
        if not person:
            return 401, ErrorResponse(message="401 Unauthorized")
        password = request.POST[PASSWORD_FIELD]
        password_hash = self.person_service.encrypt_password(password=password)
        if not self.person_service.compare_passwords_hash(password_hash, bytes(person.password_hash)):
            return 403, ErrorResponse(message="403 Forbidden")  # HttpResponse with 403 status code. Ограничение или
            # отсутствие доступа
            # к материалу на странице, которую пользователь пытается загрузить.
        access_token, expires_at = self.refresh_token_service.generate_access_token(person)
        refresh_token = self.refresh_token_service.generate_refresh_token(person)
        if refresh_token is None:
            return 401, ErrorResponse(message="Can not generate refresh token")
        return 200, JWTResponse(access_token=access_token, expires_at=expires_at, refresh_token=refresh_token.jti)

    def update_tokens(self, request: HttpRequest):
        """
        Для того чтобы получить новый access-токен, нужно запросить его с сервера, передав refresh-токен
        """
        refresh_token_jti = request.headers[REFRESH_TOKEN_FIELD]
        payload = self.refresh_token_service.decode_token(refresh_token_jti)
        refresh_token = self.refresh_token_service.get_refresh_token_by_jti(refresh_token_jti)
        if not refresh_token:  # если токена нет, то просим пользователя зарегистрироваться с помощью логина и пароля
            return 401, ErrorResponse(message="401 Unauthorized. Log in.")

        person = refresh_token.person
        valid = self.refresh_token_service.validate_token(payload, person)
        if not valid:
            self.refresh_token_service.revoke_refresh_token_by_jti(jti=refresh_token_jti)
            return 401, ErrorResponse(message="401 Unauthorized. The refresh token invalid. Log in again.")
        time_now = datetime.utcnow().timestamp()
        time_expires_refresh_token = payload[EXPIRES_AT_FIELD]
        time_delta = time_expires_refresh_token - time_now
        if time_delta < 0:
            # При попытке обновить токен уже по отозванному refresh-токену, все выданные токены пользователя
            # отзываются. Для дальнейшего доступа нужно заново авторизоваться.
            self.refresh_token_service.revoke_refresh_token_by_jti(jti=refresh_token_jti)
            return 401, ErrorResponse(message="401 Unauthorized. The refresh token has expired. Log in again.")
        access_token, expires_at = self.refresh_token_service.generate_access_token(person)
        self.refresh_token_service.revoke_refresh_token_by_jti(jti=refresh_token_jti)
        refresh_token = self.refresh_token_service.generate_refresh_token(person)
        if refresh_token is None:
            return 401, ErrorResponse(message="Can not generate refresh token")
        return 200, JWTResponse(access_token=access_token, expires_at=expires_at, refresh_token=refresh_token.jti)

    #
    # def login(self, request: HttpRequest, credentials: CredentialsSchema = Body(...)):
    #     user = self._auth_service.get_user_by_phone(credentials.phone)
    #     if not user or not check_password(credentials.password, user.password):
    #         raise UnauthorizedException("credentials are invalid")
    #     resp = self._auth_service.generate_jwt_tokens(user)
    #     return resp
    #
    # def refresh(self, request: HttpRequest, token_refresh_data: JWTRefreshSchema = Body(...)):
    #     body = json.loads(request.body.decode("utf-8"))
    #     if "refresh_token" not in body:
    #         return JsonResponse({"detail": "key refresh_token must be present"}, status=400)
    #     issued_token = self._auth_service.get_issued_token(token_refresh_data.refresh_token)
    #     if issued_token is None:
    #         raise BadRequestException("refresh token is invalid")
    #     if issued_token.revoked:
    #         self._auth_service.revoke_all_tokens(issued_token.user)
    #         raise BadRequestException("refresh token has been revoked")
    #     if self._auth_service.check_token_has_expired(token_refresh_data.refresh_token):
    #         raise BadRequestException("refresh token is expired")
    #     self._auth_service.revoke_token(issued_token)
    #     resp = self._auth_service.generate_jwt_tokens(issued_token.user)
    #     return resp


# class LogoutView(View):
#     def post(self, request: HttpRequest):
#         """
#         Выход пользователя с приложения со всех устройств. Удаление refresh-токена пользователя.
#         Дальнейший вход будет по логину и паролю.
#         """
#         person_login = request.POST[LOGIN_FIELD]
#         person = person_service.get_person_by_telegram_username(telegram_username=person_login)
#         if not person:
#             return HttpResponse("401 Unauthorized", status=HTTPStatus.UNAUTHORIZED)
#         revoked_count = refresh_token_service.revoke_refresh_token_by_person(person)
#         if revoked_count == 0:
#             return HttpResponse("401 Unauthorized", status=HTTPStatus.UNAUTHORIZED)
#         return HttpResponse(status=HTTPStatus.OK)

#
# class LoginView(View):
#     def post(self, request: HttpRequest):
#         """
#         Логика работы: пользователь вводит логин и пароль, клиент отправляет их на сервер,
#         сервер генерирует токены (access и refresh), клиент их сохраняет для пользователя.
#         После этого, юзер может не вводить пароль и логин каждый раз при входе в приложение.
#         """
#         login = request.POST[LOGIN_FIELD]
#         person = person_service.get_person_by_telegram_username(telegram_username=login)
#         if not person:
#             http_status = HTTPStatus.UNAUTHORIZED
#             return HttpResponse("401 Unauthorized", status=http_status)
#         password = request.POST[PASSWORD_FIELD]
#         password_hash = person_service.encrypt_password(password=password)
#         if not person_service.compare_passwords_hash(password_hash, bytes(person.password_hash)):
#             return HttpResponseForbidden()  # HttpResponse with 403 status code. Ограничение или отсутствие доступа
#             # к материалу на странице, которую пользователь пытается загрузить.
#         access_token, expires_at = refresh_token_service.generate_access_token(person)
#         refresh_token = refresh_token_service.generate_refresh_token(person)
#         return JsonResponse(
#             {"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token.jti}
#         )

#
# class UpdateTokensView(View):
#     def post(self, request: HttpRequest):
#         """
#         Для того чтобы получить новый access-токен, нужно запросить его с сервера, передав refresh-токен
#         """
#         refresh_token_jti = request.headers[REFRESH_TOKEN_FIELD]
#         payload = refresh_token_service.decode_token(refresh_token_jti)
#         refresh_token = refresh_token_service.get_refresh_token_by_jti(refresh_token_jti)
#         if not refresh_token:  # если токена нет, то просим пользователя зарегистрироваться с помощью логина и пароля
#             return HttpResponse("401 Unauthorized. Log in.", status=HTTPStatus.UNAUTHORIZED)
#
#         person = refresh_token.person
#         valid = refresh_token_service.validate_token(payload, person)
#         if not valid:
#             refresh_token_service.revoke_refresh_token_on_device_by_person(person=refresh_token.person,
#                                                                            device_id=refresh_token.device_id)
#             return HttpResponse(
#                 "401 Unauthorized. The refresh token invalid. Log in again.", status=HTTPStatus.UNAUTHORIZED
#             )
#         time_now = datetime.utcnow().timestamp()
#         time_expires_refresh_token = payload[EXPIRES_AT_FIELD]
#         time_delta = time_expires_refresh_token - time_now
#         if time_delta < 0:
#             # При попытке обновить токен уже по отозванному refresh-токену, все выданные токены пользователя
#             # отзываются. Для дальнейшего доступа нужно заново авторизоваться.
#             refresh_token_service.revoke_refresh_token_on_device_by_person(person=refresh_token.person,
#                                                                            device_id=refresh_token.device_id)
#             return HttpResponse(
#                 "401 Unauthorized. The refresh token has expired. Log in again.", status=HTTPStatus.UNAUTHORIZED
#             )
#         access_token, expires_at = refresh_token_service.generate_access_token(person)
#         refresh_token_service.revoke_refresh_token_on_device_by_person(person=refresh_token.person,
#                                                                        device_id=refresh_token.device_id)
#         refresh_token = refresh_token_service.generate_refresh_token(person)
#         return JsonResponse(
#             {"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token.jti}
#         )
