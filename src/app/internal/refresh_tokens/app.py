from ninja import NinjaAPI

from app.internal.refresh_tokens.data.repositories import RefreshTokenRepository
from app.internal.refresh_tokens.domain.services import RefreshTokenService

# from app.internal.refresh_tokens.http_jwt_auth import HTTJWTAuth
from app.internal.refresh_tokens.presentation.handlers import RefreshTokenHandlers
from app.internal.refresh_tokens.presentation.routers import add_refresh_token_router


def get_refresh_token_service():
    refresh_token_repo = RefreshTokenRepository()
    refresh_token_service = RefreshTokenService(refresh_token_repo=refresh_token_repo)
    return refresh_token_service


def configure_refresh_token_api(api: NinjaAPI):
    refresh_token_service = get_refresh_token_service()
    refresh_token_handler = RefreshTokenHandlers(refresh_token_service=refresh_token_service)
    return add_refresh_token_router(api, refresh_token_handler)


def get_refresh_token_handlers():
    service = get_refresh_token_service()
    return RefreshTokenHandlers(service)
