from ninja import Schema


class ErrorResponse(Schema):
    message: str


class NotFoundResponse(Schema):
    message: str


class SuccessResponse(Schema):
    success: object


class SchemaResponse(Schema):
    message: str


class JWTResponse(Schema):
    access_token: str
    expires_at: str
    refresh_token: str
