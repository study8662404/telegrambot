import re
from typing import List

from app.internal.bank_accounts.data.models import (
    BANK_NUMBER_VALIDATOR_MESSAGE,
    BANK_NUMBER_VALIDATOR_REGEX,
    MAX_BANK_NUMBER_LENGTH,
)
from app.internal.bank_accounts.data.repositories import IBankAccountRepository
from app.internal.bank_accounts.domain.entities import BankAccountOut
from app.internal.bank_accounts.presentation.messages import (
    BANK_ACCOUNT_NUMBER,
    BANK_CARD_AMOUNT,
    EXCLAMATION_MARK,
    LINE_BREAK,
    NO_BANK_ACCOUNT,
    RUB,
)
from app.internal.bank_transactions.presentation.messages import BANK_ACCOUNT_NOT_LINKED
from app.internal.persons.domain.entities import PersonIn
from app.internal.persons.domain.services import PersonService


class BankAccountService:
    def __init__(self, bank_account_repo: IBankAccountRepository):
        self._bank_account_repo = bank_account_repo

    def get_bank_account_by_owner(self, person: PersonIn) -> List[BankAccountOut] | None:
        bank_accounts = self._bank_account_repo.get_bank_account_by_owner(person)
        if bank_accounts is None:
            return None
        return [BankAccountOut.from_orm(acc) for acc in bank_accounts]

    def get_bank_account_by_number(self, number: str) -> BankAccountOut | None:
        bank_account = self._bank_account_repo.get_bank_account_by_number(number)
        return BankAccountOut.from_orm(bank_account) if bank_account is not None else None

    def get_bank_account_by_card_number(self, card_number: str) -> BankAccountOut | None:
        bank_account = self._bank_account_repo.get_bank_account_by_card_number(card_number)
        return BankAccountOut.from_orm(bank_account) if bank_account is not None else None

    def check_bank_account_number_validity(self, number: str) -> bool:
        """
        Проверка валидности номера банковского счёта.
        :param number: Номер банковской карты
        return True, если номер банковского счёта валиден. Иначе False.
        """
        return (len(number) == MAX_BANK_NUMBER_LENGTH) and (re.search(BANK_NUMBER_VALIDATOR_REGEX, number) is not None)

    def form_message_on_account_balance_command(
        self,
        person_service: PersonService,
        indicate_account_number: bool,
        effective_user_id: int,
        valid: bool,
        account_number: str,
    ):
        """
        Формирование сообщения для пользователя в ответ на команду "account_balance".
        :param indicate_account_number: Логическая переменная, был ли указан в сообщении номер банковского счёта;
        :param effective_user_id: уникальный идентификатор сообщений пользователя;
        :param valid: Логическая переменная, является ли номер банковского счёта, переданный в сообщении, валидным
        :param account_number: номер банковского счёта.
        :return: Возвращает сообщение пользователю в ответ на команду "account_balance".
        """
        message = ""
        bank_accounts = None
        person = person_service.get_person(effective_user_id)
        if not indicate_account_number:
            bank_accounts = self.get_bank_account_by_owner(person)
            if len(bank_accounts) == 0:
                message = f"{person.firstname}{EXCLAMATION_MARK}{NO_BANK_ACCOUNT}"
            else:  # у пользователя может быть несколько банковских аккаунтов
                message_list = [f"{person.firstname}{EXCLAMATION_MARK}"]
                for account in bank_accounts:
                    message_list.append(
                        f"{BANK_ACCOUNT_NUMBER}{account.number}. {BANK_CARD_AMOUNT}{account.amount}{RUB}"
                    )
                message = LINE_BREAK.join(message_list)
        else:
            if not valid:
                message = f"{person.firstname}{EXCLAMATION_MARK}{BANK_NUMBER_VALIDATOR_MESSAGE}"
            else:
                bank_account = self.get_bank_account_by_number(account_number)
                if bank_account is None:
                    message = f"{person.firstname}{EXCLAMATION_MARK}{BANK_ACCOUNT_NUMBER}{account_number}{BANK_ACCOUNT_NOT_LINKED}"
                else:
                    message = (
                        f"{person.firstname}{EXCLAMATION_MARK}{BANK_ACCOUNT_NUMBER}{bank_account.number}."
                        f"{BANK_CARD_AMOUNT}{bank_account.amount} {RUB}"
                    )
        return message
