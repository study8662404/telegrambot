from _decimal import Decimal
from ninja.orm import create_schema

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.persons.domain.entities import PersonSchema

BankAccountSchema = create_schema(BankAccount)


class BankAccountIn(BankAccountSchema):
    account_owner: PersonSchema
    number: str
    amount: Decimal


class BankAccountOut(BankAccountSchema):
    account_owner: PersonSchema
    number: str
    amount: Decimal
