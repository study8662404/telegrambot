from typing import List

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.persons.data.models import Person


class IBankAccountRepository:
    def get_bank_account_by_owner(self, person: Person) -> List[BankAccount] | None:
        ...

    def get_bank_account_by_number(self, number: str) -> BankAccount | None:
        ...

    def get_bank_account_by_card_number(self, card_number: str) -> BankAccount | None:
        ...
