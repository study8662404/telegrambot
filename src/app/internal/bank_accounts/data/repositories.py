from typing import List

from app.internal.bank_accounts.data.models import BankAccount
from app.internal.bank_accounts.domain.repo_intreface import IBankAccountRepository
from app.internal.persons.data.models import Person


class BankAccountRepository(IBankAccountRepository):
    def get_bank_account_by_owner(self, person: Person) -> List[BankAccount] | None:
        """
        Получение объекта банковского счёта с помощью информации о его владельце.
        :param person: Владелец банковского счёта.
        :return: Объект банковского счёта, принадлежащий пользователю,
        переданного в качестве параметра метода, если такой счёт существует.
        Иначе None.
        """
        bank_accounts = BankAccount.objects.select_related().filter(account_owner__telegram_id=person.telegram_id)
        if bank_accounts is None:
            return None
        return list(bank_accounts)  # равносильно
        # select * from BankAccount where account_owner = Person;

    def get_bank_account_by_number(self, number: str) -> BankAccount | None:
        """
        Получение объекта банковского счёта по его номеру.
        :param number: Номер банковского счёта.
        :return: Объект банковского счёта с номером, переданным в качестве параметра метода, если такой существует.
        Иначе None.
        """
        return BankAccount.objects.select_related().filter(number=number).first()  # равносильно
        # select * from BankAccount where number = number;

    def get_bank_account_by_card_number(self, card_number: str) -> BankAccount | None:
        """
        Получение объекта банковского счёта привязанного к банковской карте.
        :param card_number: Номер банковской карты.
        :return: Объект банковского счёта, привязанного к банковской карте, если такой существует. Иначе None.
        """
        return BankAccount.objects.select_related().filter(bankcard__card_number=card_number).first()
