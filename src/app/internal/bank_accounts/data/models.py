from django.core.validators import MinValueValidator, RegexValidator
from django.db import models

from app.internal.persons.data.models import Person

MAX_BANK_NUMBER_LENGTH = 20  # см BANK_NUMBER_VALIDATOR_MESSAGE

BANK_NUMBER_VALIDATOR_MESSAGE = (
    "Проверьте, пожалуйста, количество цифр вашего банковского счёта!"
    " В России номер банковского счёта содержит 20 цифр."
)

BANK_NUMBER_VALIDATOR_REGEX = f"\\d{{{MAX_BANK_NUMBER_LENGTH}}}"  # Шаблон регулярного выражения:
# MAX_BANK_NUMBER_LENGTH десятичных цифр
# в номере счёта, внешние {} т.к. форматированный вывод, внутренние {} т.к. синтаксис шаблона регулярного выражения,
# еще одни внутренние {} --- экранирование

bank_number_validator = RegexValidator(  # Валидатор - это вызываемый объект,
    # который принимает значение и выдает ValidationError,
    # если он не соответствует некоторым критериям.
    # Проверяет, что введенный номер банковского счёта содержит только цифры в количестве MAX_BANK_NUMBER_LENGTH
    regex=BANK_NUMBER_VALIDATOR_REGEX,
    message=BANK_NUMBER_VALIDATOR_MESSAGE,
)


class BankAccount(models.Model):
    """
    Данный класс представляет собой модель банковского аккаунта пользователя Telegram бота.
    Содержит поля и параметры хранения данных: номер банковского счёта, информацию о владельце, количество средств.
    Сопоставляется с одной таблицей базы данных.
    """

    number = models.CharField(  # Номер банковского счёта содержит
        # только цифры, максимальная длина составляет 20 символов. BigIntegerField не подходит т.к. содержит
        # максимально только ~ 9^18 цифр.
        max_length=MAX_BANK_NUMBER_LENGTH,
        validators=[bank_number_validator],
        unique=True,  # является первичным ключом таблицы
        verbose_name="Номер банковского счёта",
    )

    account_owner = models.ForeignKey(
        Person,  # является внешним ключом таблицы
        on_delete=models.CASCADE,  # при удалении пользователя, банковский аккаунт удаляется
        verbose_name="Владелец банковского счёта",
    )
    amount = models.DecimalField(
        max_digits=11,  # Максимальное значение целой части: 9*10^(11-2) т.к. 2 знака выделено под хранение дробной
        # части
        decimal_places=2,  # Количество знаков выделенное под хранение дробной части
        validators=[MinValueValidator(limit_value=0)],  # ограничение на минимальную сумму - 0.
        verbose_name="Количество средств на банковском счету",
    )
    objects = models.Manager()  # менеджер объектов

    def __str__(self):
        return f"account: {self.account_owner}, {self.number}"

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели банковского счёта.
        """

        verbose_name = "Банковский счёт"  # Удобочитаемое имя для поля, единственное число
        verbose_name_plural = "Банковские счета"  # Удобочитаемое имя для поля, множественное число
