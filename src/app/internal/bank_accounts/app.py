from ninja import NinjaAPI

from app.internal.bank_accounts.data.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_accounts.presentation.handlers import BankAccountHandlers
from app.internal.bank_accounts.presentation.routers import add_bank_account_router
from app.internal.persons.app import get_person_service
from app.internal.persons.domain.services import PersonService


def get_bank_account_service():
    bank_account_repo = BankAccountRepository()
    bank_account_service = BankAccountService(bank_account_repo=bank_account_repo)
    return bank_account_service


def configure_bank_account_api(api: NinjaAPI):
    bank_account_service = get_bank_account_service()
    person_service = get_person_service()
    bank_account_handler = BankAccountHandlers(bank_account_service=bank_account_service, person_service=person_service)
    add_bank_account_router(api, bank_account_handler)


def get_bank_account_handlers():
    bank_account_service = get_bank_account_service()
    person_service = get_person_service()
    bank_card_handler = BankAccountHandlers(bank_account_service=bank_account_service, person_service=person_service)
    return bank_card_handler
