from ninja import Body, Path
from telegram import Update
from telegram.ext import CallbackContext

from app.bot.services import send_message
from app.internal.bank_accounts.domain.entities import BankAccountOut
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.broad.responses import NotFoundResponse, SuccessResponse
from app.internal.persons.domain.entities import PersonIn
from app.internal.persons.domain.services import PersonService


class BankAccountHandlers:
    def __init__(self, bank_account_service: BankAccountService, person_service: PersonService):
        self.bank_account_service = bank_account_service
        self.person_service = person_service

    def get_bank_account_by_owner(self, request, person: PersonIn = Body(...)):
        bank_accounts = self.bank_account_service.get_bank_account_by_owner(person)
        if bank_accounts is None:
            return 404, NotFoundResponse(message="Unknown owner")
        return 200, SuccessResponse(success=[BankAccountOut.from_orm(acc) for acc in bank_accounts])

    def get_bank_account_by_number(self, request, number: str = Path(...)):
        bank_account = self.bank_account_service.get_bank_account_by_number(number)
        if bank_account is None:
            return 404, NotFoundResponse(message="Unknown number")
        return 200, SuccessResponse(success=BankAccountOut.from_orm(bank_account))

    def get_bank_account_by_card_number(self, request, card_number: str = Path(...)):
        bank_account = self.bank_account_service.get_bank_account_by_card_number(card_number)
        if bank_account is None:
            return 404, NotFoundResponse(message="Unknown card number")
        return 200, SuccessResponse(success=BankAccountOut.from_orm(bank_account))

    def account_balance(self, update: Update, context: CallbackContext) -> None:
        """
        Функция обратного вызова команды 'account_balance'. Вывод количества средств на банковском счету пользователя.
        :param update: объект, который представляет собой пришедшее обновление для бота от пользователя.
        :param context: контекст принятого сообщения.
        :return: Метод ничего не возвращает.
        """
        indicate_account_number = len(context.args) == 1
        valid = False
        account_number = ""
        if indicate_account_number:
            account_number = context.args[0]
            valid = self.bank_account_service.check_bank_account_number_validity(account_number)
        message = self.bank_account_service.form_message_on_account_balance_command(
            self.person_service, indicate_account_number, update.effective_user.id, valid, account_number
        )
        send_message(context, update.effective_chat.id, message)
