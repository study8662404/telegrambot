BANK_ACCOUNT_NUMBER = "\nНомер банковского счёта: "
BANK_ACCOUNT_AMOUNT = "\nКоличество средств на карте составляет "
BANK_CARD_AMOUNT = "\nКоличество средств на вашем банковском счёте составляет "
NO_BANK_ACCOUNT = "\nУ вас ещё нет банковского счёта."
RUB = " рублей"
EXCLAMATION_MARK = "!"  # восклицательный знак
LINE_BREAK = "\n"
