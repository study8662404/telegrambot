from django.contrib import admin

from app.internal.bank_accounts.data.models import BankAccount


@admin.register(BankAccount)  # декоратор для регистрации классов ModelAdmin
class BankAccountAdmin(admin.ModelAdmin):
    """
    Данный класс является представлением модели BankAccount в интерфейсе администрирования.
    """

    fields = ["number", "account_owner", "amount"]
