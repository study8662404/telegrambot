from ninja import NinjaAPI, Router

from app.internal.bank_accounts.domain.entities import BankAccountOut
from app.internal.bank_accounts.presentation.handlers import BankAccountHandlers
from app.internal.broad.responses import NotFoundResponse, SuccessResponse


def get_bank_account_router(bank_account_handlers: BankAccountHandlers):
    router = Router()
    router.add_api_operation(
        "get/{str:number}",
        ["GET"],
        bank_account_handlers.get_bank_account_by_number,
        response={404: NotFoundResponse, 200: SuccessResponse},
    )

    router.add_api_operation(
        "get_by_card_number/{str:card_number}",
        ["GET"],
        bank_account_handlers.get_bank_account_by_card_number,
        response={200: SuccessResponse, 404: NotFoundResponse},
    )
    return router


def add_bank_account_router(api: NinjaAPI, bank_account_handlers: BankAccountHandlers):
    bank_account = get_bank_account_router(bank_account_handlers)
    api.add_router("bank_account", bank_account)
