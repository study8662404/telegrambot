from django.urls import path

from app.internal.bank_accounts.app import configure_bank_account_api
from app.internal.persons.app import configure_person_api
from app.internal.refresh_tokens.app import configure_refresh_token_api

urlpatterns = []
