from app.internal.postcards.data.repositories import PostCardRepository
from app.internal.postcards.domain.services import PostCardService
from app.internal.postcards.presentation.handlers import PostCardHandlers


def get_postcard_service():
    post_card_repo = PostCardRepository()
    post_card_service = PostCardService(post_card_repo)
    return post_card_service


def get_postcard_handlers():
    service = get_postcard_service()
    return PostCardHandlers(service)
