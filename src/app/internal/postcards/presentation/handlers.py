from app.internal.postcards.domain.services import PostCardService


class PostCardHandlers:
    def __init__(self, postcard_service: PostCardService):
        self._postcard_service = postcard_service
