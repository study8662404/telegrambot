from django.contrib import admin

from app.internal.postcards.data.models import PostCard


@admin.register(PostCard)
class PostCardAdmin(admin.ModelAdmin):
    fields = ("content",)
