from django.db import models


class PostCard(models.Model):
    """
    Данный класс представляет собой модель открытки(изображения), которая прикрепляется к переводу через Telegram бота.
    Содержит поля и параметры хранения данных.
    Сопоставляется с одной таблицей базы данных.
    """

    content = models.FileField()

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        """
        Контейнер класса с метаданными, прикрепленными к модели открытки транзакции PostCards
        """

        verbose_name = "Открытка"
        verbose_name_plural = "Открытки"
