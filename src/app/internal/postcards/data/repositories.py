from django.core.files.base import ContentFile

from app.internal.bank_transactions.domain.entities import BankTransactionOut
from app.internal.postcards.data.models import PostCard
from app.internal.postcards.domain.entities import PostCardIn
from app.internal.postcards.domain.repo_interface import IPostCardRepository


class PostCardRepository(IPostCardRepository):
    def get_post_card_by_transaction(self, bank_transaction: BankTransactionOut):
        return PostCard.objects.filter(id=bank_transaction.postcard.id).first()

    def get_postcard_by_id(self, postcard_id):
        return PostCard.objects.filter(pk=postcard_id).first()

    def create_post_card(self, file_name, content):
        postcard_in = PostCardIn(content=content)
        post_card_data_dict = postcard_in.dict()
        post_card_data_dict["content"] = ContentFile(postcard_in.content, file_name)
        postcard = PostCard.objects.create(**post_card_data_dict)
        return postcard
