from ninja import Schema
from pydantic import Field


class PostCardSchema(Schema):
    content: bytes = Field(b"")


class PostCardOut(Schema):
    id: int
    content: bytes = Field(b"")

    class Config:
        fields = {"id": "pk"}


class PostCardIn(PostCardSchema):
    ...
