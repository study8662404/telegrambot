from app.internal.bank_transactions.domain.entities import BankTransactionOut


class IPostCardRepository:
    def create_post_card(self, file_name, content):
        ...

    def get_post_card_by_transaction(self, bank_transaction: BankTransactionOut):
        ...

    def get_postcard_by_id(self, postcard_id):
        ...
