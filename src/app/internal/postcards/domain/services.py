from app.internal.bank_transactions.domain.entities import BankTransactionOut
from app.internal.postcards.domain.entities import PostCardOut
from app.internal.postcards.domain.repo_interface import IPostCardRepository


class PostCardService:
    def __init__(self, postcard_repo: IPostCardRepository):
        self._postcard_repo = postcard_repo

    def create_post_card(self, file_name, content):
        postcard = self._postcard_repo.create_post_card(file_name, content)
        return PostCardOut.from_orm(postcard)

    def get_post_card_by_transaction(self, bank_transaction: BankTransactionOut):
        return self._postcard_repo.get_post_card_by_transaction(bank_transaction)

    def get_postcard_by_id(self, postcard_id):
        return self._postcard_repo.get_postcard_by_id(postcard_id)
