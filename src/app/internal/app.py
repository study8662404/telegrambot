from ninja import NinjaAPI

from app.internal.bank_accounts.app import configure_bank_account_api
from app.internal.persons.app import configure_person_api
from app.internal.refresh_tokens.app import configure_refresh_token_api
from app.internal.refresh_tokens.http_jwt_auth import HTTJWTAuth


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
    )
    configure_person_api(api)
    configure_bank_account_api(api)
    configure_refresh_token_api(api)
    api.auth = HTTJWTAuth()
    # api = NinjaAPI(
    #     title="DT.EDU.BACKEND",
    #     version="1.0.0",
    #     # csrf=False,
    #     auth=[HTTJWTAuth()],
    # )
    return api
