from django.core.management.base import BaseCommand

from app.bot.polling_bot import PollingBot
from app.bot.webhook_bot import WebhookBot
from config.settings import DO_POLLING


class Command(BaseCommand):
    """
    Класс команды управления ботом.
    Является management command. Management commands — команды, выполняемые из командной строки с
    помощью скрипта manage.py.
    """

    help = "Запуск VelBot"  # содержит вспомогательную информацию о команде.

    def handle(self, *args, **options):
        """
        Отвечает за запуск бота.
        :param args: аргументы, переданные при запуске из интерпретатора командной строки.
        :param options: опции, указанные при запуске из интерпретатора командной строки.
        :return: Отсутствуют выходные значения.
        """
        if DO_POLLING:
            bot = PollingBot()
            bot.run()
        else:
            bot = WebhookBot()
            bot.run()
